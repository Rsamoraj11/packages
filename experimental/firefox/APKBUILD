# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=firefox
pkgver=66.0.2
pkgrel=0
pkgdesc="Firefox Web browser (unstable)"
url="https://www.mozilla.org/firefox/"
arch="all"
options="!check"  # Tests disabled
license="MPL-2.0"
depends=""
# moz build system stuff
# python deps
# system-libs
# actual deps
makedepends="
	autoconf2.13 cargo clang llvm7-dev node ncurses-dev perl rust
	rust-stdlib cmd:which

	ncurses-dev openssl-dev

	alsa-lib-dev bzip2-dev icu-dev libevent-dev libffi-dev libpng-dev
	libjpeg-turbo-dev nspr-dev nss-dev pulseaudio-dev zlib-dev

	dbus-glib-dev gconf-dev gtk+3.0-dev hunspell-dev libnotify-dev
	libsm-dev libxcomposite-dev libxdamage-dev libxrender-dev libxt-dev
	nss-static sqlite-dev startup-notification-dev unzip yasm zip
	gtk+2.0-dev
	"
_py2ver="2.7.15"
source="https://ftp.mozilla.org/pub/firefox/releases/$pkgver/source/firefox-$pkgver.source.tar.xz
	https://www.python.org/ftp/python/$_py2ver/Python-$_py2ver.tar.xz
	mozconfig

	rust-config.patch
	"

unpack() {
	default_unpack
	[ -z $SKIP_PYTHON ] || return 0

	msg "Killing all remaining hope for humanity and building Python 2..."
	cd "$srcdir/Python-$_py2ver"
	[ -d ../python ] && rm -r ../python
	# 19:39 <+solar> just make the firefox build process build its own py2 copy
	# 20:03 <calvin> TheWilfox: there's always violence
	./configure --prefix="$srcdir/python"
	make -j $JOBS
	# 6 tests failed:
	#    test__locale test_os test_posix test_re test_strptime test_time
	# make test
	make -j $JOBS install
}

prepare() {
	cd "$builddir"
	default_prepare
	cp "$srcdir"/mozconfig "$builddir"/mozconfig
	echo "ac_add_options --enable-optimize=\"$CFLAGS\"" >> "$builddir"/mozconfig
	echo "ac_add_options --host=\"$CHOST\"" >> "$builddir"/mozconfig
	echo "ac_add_options --target=\"$CTARGET\"" >> "$builddir"/mozconfig
	# too much memory
	if [ -z "$JOBS" -o $JOBS -gt 16 ]; then
		echo "mk_add_options MOZ_MAKE_FLAGS=\"-j16\"" >> "$builddir"/mozconfig
	fi
}

build() {
	cd "$builddir"

	export SHELL=/bin/sh
	export BUILD_OFFICIAL=1
	export MOZILLA_OFFICIAL=1
	export USE_SHORT_LIBNAME=1
	# gcc 6
	export CXXFLAGS="-fno-delete-null-pointer-checks -fno-schedule-insns2"

	# set rpath so linker finds the libs
	export LDFLAGS="$LDFLAGS -Wl,-rpath,${_mozappdir}"

	local extra_flags=""
	[ "$CARCH" = "s390x" ] && extra_flags="--disable-startupcache"

	export PATH="$srcdir/python/bin:$PATH"
	./mach build
}

run() {
	cd "$builddir"/obj-$CHOST/dist/bin
	export LD_LIBRARY_PATH=.
	./firefox -no-remote -profile "$builddir"/obj-$CHOST/tmp/profile-default
}

package() {
	cd "$builddir"
	export PATH="$srcdir/python/bin:$PATH"
	DESTDIR="$pkgdir" ./mach install

	install -m755 -d ${pkgdir}/usr/share/applications
	install -m755 -d ${pkgdir}/usr/share/pixmaps

	local png
	for png in browser/branding/official/default*.png; do
		local i=${_png%.png}
		i=${i##*/default}
		install -D -m644 "$png" \
			"$pkgdir"/usr/share/icons/hicolor/${i}x${i}/apps/firefox.png
	done

	install -m644 "$builddir"/browser/branding/official/default48.png \
		${pkgdir}/usr/share/pixmaps/firefox.png
	install -m644 ${startdir}/firefox.desktop \
		${pkgdir}/usr/share/applications/firefox.desktop
	install -m644 ${startdir}/firefox-safe.desktop \
		${pkgdir}/usr/share/applications/firefox-safe.desktop

	# install our vendor prefs
	install -d "$pkgdir"/$_mozappdir/browser/defaults/preferences

	cat >> "$pkgdir"/$_mozappdir/browser/defaults/preferences/firefox-branding.js <<- EOF
	// Use LANG environment variable to choose locale
	pref("intl.locale.matchOS", true);

	// Disable default browser checking.
	pref("browser.shell.checkDefaultBrowser", false);

	// Don't disable our bundled extensions in the application directory
	pref("extensions.autoDisableScopes", 11);
	pref("extensions.shownSelectionUI", true);
	EOF
}

sha512sums="2246f3aed4e8a1d557a2383204e926c45d63d7977032512982b3698da4d6260b67fa91507f1a103af0af77198b7ffb34d6609159729aa7bf1c7c16cf2f7efc5b  firefox-66.0.2.source.tar.xz
27ea43eb45fc68f3d2469d5f07636e10801dee11635a430ec8ec922ed790bb426b072da94df885e4dfa1ea8b7a24f2f56dd92f9b0f51e162330f161216bd6de6  Python-2.7.15.tar.xz
dde70694cd3b3abfcac8d0737bf6031a96918a1b0a1846e5aa7b246511621417e2710acc2f783f77edc3f0043e67b0822ef9bdd4d6034729c432f9752d8f1f6d  mozconfig
fd3b165e26ab931dd7eaf220d578c30b8772eab0a870710d59403c9823c2233ef941cd7eb25d1625d705de9e8a7138d0e8c4e227a185e9b687553132da96d81a  rust-config.patch"
