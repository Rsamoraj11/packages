# HG changeset patch
# User Lee Salzman <lsalzman@mozilla.com>
# Date 1527131721 14400
#      Wed May 23 23:15:21 2018 -0400
# Node ID a8d83bebeab4c07a850711aa2c241a56f784613e
# Parent  50fbb3e601fef35e2a673895cb0b3bc05374458a
fix big-endian Skia builds

MozReview-Commit-ID: JQivGBE45qy

diff --git a/gfx/skia/skia/include/core/SkColorPriv.h b/gfx/skia/skia/include/core/SkColorPriv.h
--- a/gfx/skia/skia/include/core/SkColorPriv.h
+++ b/gfx/skia/skia/include/core/SkColorPriv.h
@@ -50,27 +50,20 @@ static inline U8CPU SkUnitScalarClampToB
  *  For easier compatibility with Skia's GPU backend, we further restrict these
  *  to either (in memory-byte-order) RGBA or BGRA. Note that this "order" does
  *  not directly correspond to the same shift-order, since we have to take endianess
  *  into account.
  *
  *  Here we enforce this constraint.
  */
 
-#ifdef SK_CPU_BENDIAN
-    #define SK_RGBA_R32_SHIFT   24
-    #define SK_RGBA_G32_SHIFT   16
-    #define SK_RGBA_B32_SHIFT   8
-    #define SK_RGBA_A32_SHIFT   0
-#else
-    #define SK_RGBA_R32_SHIFT   0
-    #define SK_RGBA_G32_SHIFT   8
-    #define SK_RGBA_B32_SHIFT   16
-    #define SK_RGBA_A32_SHIFT   24
-#endif
+#define SK_RGBA_R32_SHIFT   0
+#define SK_RGBA_G32_SHIFT   8
+#define SK_RGBA_B32_SHIFT   16
+#define SK_RGBA_A32_SHIFT   24
 
 #define SkGetPackedA32(packed)      ((uint32_t)((packed) << (24 - SK_A32_SHIFT)) >> 24)
 #define SkGetPackedR32(packed)      ((uint32_t)((packed) << (24 - SK_R32_SHIFT)) >> 24)
 #define SkGetPackedG32(packed)      ((uint32_t)((packed) << (24 - SK_G32_SHIFT)) >> 24)
 #define SkGetPackedB32(packed)      ((uint32_t)((packed) << (24 - SK_B32_SHIFT)) >> 24)
 
 #define SkA32Assert(a)  SkASSERT((unsigned)(a) <= SK_A32_MASK)
 #define SkR32Assert(r)  SkASSERT((unsigned)(r) <= SK_R32_MASK)
diff --git a/gfx/skia/skia/include/core/SkImageInfo.h b/gfx/skia/skia/include/core/SkImageInfo.h
--- a/gfx/skia/skia/include/core/SkImageInfo.h
+++ b/gfx/skia/skia/include/core/SkImageInfo.h
@@ -79,17 +79,17 @@ enum SkColorType {
 
     kLastEnum_SkColorType = kRGBA_F16_SkColorType,
 
 #if SK_PMCOLOR_BYTE_ORDER(B,G,R,A)
     kN32_SkColorType = kBGRA_8888_SkColorType,
 #elif SK_PMCOLOR_BYTE_ORDER(R,G,B,A)
     kN32_SkColorType = kRGBA_8888_SkColorType,
 #else
-    #error "SK_*32_SHIFT values must correspond to BGRA or RGBA byte order"
+    kN32_SkColorType = kBGRA_8888_SkColorType,
 #endif
 };
 
 /**
  *  Returns the number of bytes-per-pixel for the specified colortype, or 0 if invalid.
  */
 SK_API int SkColorTypeBytesPerPixel(SkColorType ct);
 
diff --git a/gfx/skia/skia/include/gpu/GrTypes.h b/gfx/skia/skia/include/gpu/GrTypes.h
--- a/gfx/skia/skia/include/gpu/GrTypes.h
+++ b/gfx/skia/skia/include/gpu/GrTypes.h
@@ -339,25 +339,22 @@ enum GrPixelConfig {
     kPrivateConfig4_GrPixelConfig,
     kPrivateConfig5_GrPixelConfig,
 
     kLast_GrPixelConfig = kPrivateConfig5_GrPixelConfig
 };
 static const int kGrPixelConfigCnt = kLast_GrPixelConfig + 1;
 
 // Aliases for pixel configs that match skia's byte order.
-#ifndef SK_CPU_LENDIAN
-    #error "Skia gpu currently assumes little endian"
-#endif
 #if SK_PMCOLOR_BYTE_ORDER(B,G,R,A)
     static const GrPixelConfig kSkia8888_GrPixelConfig = kBGRA_8888_GrPixelConfig;
 #elif SK_PMCOLOR_BYTE_ORDER(R,G,B,A)
     static const GrPixelConfig kSkia8888_GrPixelConfig = kRGBA_8888_GrPixelConfig;
 #else
-    #error "SK_*32_SHIFT values must correspond to GL_BGRA or GL_RGBA format."
+    static const GrPixelConfig kSkia8888_GrPixelConfig = kBGRA_8888_GrPixelConfig;
 #endif
 
 /**
  * Optional bitfield flags that can be set on GrSurfaceDesc (below).
  */
 enum GrSurfaceFlags {
     kNone_GrSurfaceFlags = 0x0,
     /**
diff --git a/gfx/skia/skia/src/core/SkColorData.h b/gfx/skia/skia/src/core/SkColorData.h
--- a/gfx/skia/skia/src/core/SkColorData.h
+++ b/gfx/skia/skia/src/core/SkColorData.h
@@ -27,27 +27,20 @@
  *  For easier compatibility with Skia's GPU backend, we further restrict these
  *  to either (in memory-byte-order) RGBA or BGRA. Note that this "order" does
  *  not directly correspond to the same shift-order, since we have to take endianess
  *  into account.
  *
  *  Here we enforce this constraint.
  */
 
-#ifdef SK_CPU_BENDIAN
-    #define SK_BGRA_B32_SHIFT   24
-    #define SK_BGRA_G32_SHIFT   16
-    #define SK_BGRA_R32_SHIFT   8
-    #define SK_BGRA_A32_SHIFT   0
-#else
-    #define SK_BGRA_B32_SHIFT   0
-    #define SK_BGRA_G32_SHIFT   8
-    #define SK_BGRA_R32_SHIFT   16
-    #define SK_BGRA_A32_SHIFT   24
-#endif
+#define SK_BGRA_B32_SHIFT   0
+#define SK_BGRA_G32_SHIFT   8
+#define SK_BGRA_R32_SHIFT   16
+#define SK_BGRA_A32_SHIFT   24
 
 #if defined(SK_PMCOLOR_IS_RGBA) && defined(SK_PMCOLOR_IS_BGRA)
     #error "can't define PMCOLOR to be RGBA and BGRA"
 #endif
 
 #define LOCAL_PMCOLOR_SHIFTS_EQUIVALENT_TO_RGBA  \
     (SK_A32_SHIFT == SK_RGBA_A32_SHIFT &&    \
      SK_R32_SHIFT == SK_RGBA_R32_SHIFT &&    \
