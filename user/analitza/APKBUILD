# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=analitza
pkgver=19.04.2
pkgrel=0
pkgdesc="Mathematical object library"
url="https://api.kde.org/4.x-api/kdeedu-apidocs/analitza/html/index.html"
arch="all"
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev eigen-dev
	qt5-qtsvg-dev qt5-qtdeclarative-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/analitza-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(export|curve|plots|surface)'
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="208df8b0c1a47dcddfa5dc19cab96d4686b8ead8c674ad246819e3920bde4ae41ab26b656322e99996de5bc393bf9952223663e6b5f72929bd2a1619524d6c79  analitza-19.04.2.tar.xz"
