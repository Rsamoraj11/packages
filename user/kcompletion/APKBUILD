# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcompletion
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for implementing automatic completion of input"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev kwidgetsaddons-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcompletion-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# All other tests require X11.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -R ksortablelisttest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="af813411090d25f85d1b409bd971d9f610cc7c1ce01463f70cf57791af9dafe88957d74be0b63c4ca8f7d2a790c48d34613a03bed03552cae30346576849f654  kcompletion-5.54.0.tar.xz"
