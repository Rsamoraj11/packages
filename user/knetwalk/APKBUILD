# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=knetwalk
pkgver=19.04.2
pkgrel=0
pkgdesc="Build up a computer network by placing the wires correctly"
url="https://www.kde.org/applications/games/knetwalk/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev kdoctools-dev
	kdbusaddons-dev ki18n-dev ktextwidgets-dev kwidgetsaddons-dev
	kxmlgui-dev libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/knetwalk-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="95b43e03684f9a4926c4664877e2b3ea259beb8a9edbee8e1716d0beba7366e2c5c0fdac94f3611b2532354e0b7a09f3973d62bdf8d391bc87878c68638fd7ed  knetwalk-19.04.2.tar.xz"
