# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=lskat
pkgver=19.04.2
pkgrel=0
pkgdesc="Lieutenant Skat is an interactive two-player card game"
url="https://games.kde.org/game.php?game=lskat"
arch="all"
license="GPL-2.0-only"
depends="libkdegames-carddecks"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev
	kdbusaddons-dev kdoctools-dev kguiaddons-dev ki18n-dev kxmlgui-dev
	kwidgetsaddons-dev libkdegames-dev phonon-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/lskat-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="8597e88211d8108b358aa8ad1053dfaff09b2d39e5b6c74db3ca467ad4dd9ac9dcce60f386239eddb8fc7960b59f07f5f368eddae0e0ef3dd436b87257f1017b  lskat-19.04.2.tar.xz"
