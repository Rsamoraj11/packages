# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=r
pkgver=3.5.1
pkgrel=1
pkgdesc="Environment for statistical computing and graphics"
url="https://www.r-project.org/"
arch="all"
options="!check"  # 20.482886 != 20.482887
license="GPL-2.0-only"
depends="cmd:which"
makedepends="byacc bzip2-dev cairo-dev curl-dev gfortran icu-dev libice-dev
	libtirpc-dev libx11-dev libxt-dev pango-dev pcre-dev pcre2-dev xz-dev
	zlib-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://rweb.crmda.ku.edu/cran/src/base/R-3/R-$pkgver.tar.gz"
builddir="$srcdir/R-$pkgver"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--docdir=/usr/share/doc/R-$pkgver \
		--localstatedir=/var \
		--with-readline=no \
		--enable-R-shlib
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="382cc6e200469dd168799948edcf3a0b869d7ef3b3176fdfc60752f3f37e6ba356323c47d8815a4d9ab6ad3a21cd045d26ef5e75107c8685328e0ffcfd172f7f  R-3.5.1.tar.gz"
