# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=dolphin
pkgver=19.04.2
pkgrel=0
pkgdesc="Lightweight desktop file manager"
url="https://www.kde.org/applications/system/dolphin/"
arch="all"
options="!check"  # Tests require X11 and D-Bus session bus.
license="GPL-2.0-only"
depends="ffmpegthumbs kdegraphics-thumbnailers"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdoctools-dev kinit-dev
	kcmutils-dev knewstuff-dev kcoreaddons-dev ki18n-dev kdbusaddons-dev
	kbookmarks-dev kconfig-dev kio-dev kparts-dev solid-dev kiconthemes-dev
	kcompletion-dev ktextwidgets-dev knotifications-dev kcrash-dev
	baloo-dev kfilemetadata-dev kdelibs4support-dev kactivities-dev
	baloo-widgets-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/dolphin-$pkgver.tar.xz
	frameworks.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="e1480c2f98f06a29048683180e1ab30992e57a4cefde1b3a1a0da1c1c654482cc2e57d23fb7a7b8102e76f86c2b09b09900d37dbddc753c85f545146d47c5574  dolphin-19.04.2.tar.xz
4ec4b440f5ed52439152fb6132448e674f8e357413ef1eae339cfefb602a82909bceba85f5f517b51a985e3101d6be42b25bee6a4dd338a190d995deef57c708  frameworks.patch"
