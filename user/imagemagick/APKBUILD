# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: 
pkgname=imagemagick
pkgver=7.0.8.48
_abiver=7
_pkgver=${pkgver%.*}-${pkgver##*.}
pkgrel=0
pkgdesc="A collection of tools and libraries for many image formats"
url="http://www.imagemagick.org/"
arch="all"
options="libtool !check"  # needs actual helvetica font
license="Apache-2.0"
makedepends="zlib-dev libpng-dev libjpeg-turbo-dev freetype-dev fontconfig-dev
	perl-dev libwebp-dev libtool tiff-dev lcms2-dev fftw-dev libwebp-dev
	libxml2-dev librsvg-dev libraw-dev"
checkdepends="freetype fontconfig lcms2 graphviz"
subpackages="$pkgname-doc $pkgname-dev $pkgname-c++:_cxx $pkgname-libs"
source="https://distfiles.adelielinux.org/source/ImageMagick-$_pkgver.tar.xz"
builddir="$srcdir/ImageMagick-${_pkgver}"

build() {
	cd "$builddir"
	# fix doc dir, Gentoo bug 91911
	sed -i -e \
		's:DOCUMENTATION_PATH="${DATA_DIR}/doc/${DOCUMENTATION_RELATIVE_PATH}":DOCUMENTATION_PATH="/usr/share/doc/imagemagick":g' \
		configure
	local _openmp=
	case "$CARCH" in
	s390x) _openmp="--disable-openmp"
	esac

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-static \
		$_openmp \
		--with-threads \
		--with-x \
		--with-tiff \
		--with-png \
		--with-webp \
		--with-rsvg \
		--without-gslib \
		--with-modules \
		--with-xml \
		$_pic
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
	if ! [ -e "$pkgdir"/usr/lib/libMagickCore-$_abiver.Q16HDRI.so ]; then
		error "Has ABI verision changed? (current is $_abiver)"
		return 1
	fi

	# we cannot let abuild delete the *.la files due to we need *.la
	# for the modules
	rm "$pkgdir"/usr/lib/*.la

	find "$pkgdir" -name '.packlist' -o -name 'perllocal.pod' \
		-o -name '*.bs' -delete

	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

_cxx() {
	pkgdesc="ImageMagick Magick++ library (C++ bindings)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libMagick++*.so.* "$subpkgdir"/usr/lib/
}

sha512sums="194773526b86762fbcd509a722114c88b23a7bdd96431f666581e11d2c13334e6f58c1edcecb6e9117a00ea873cca8657887f7962ccbb5748d1c692b7756c792  ImageMagick-7.0.8-48.tar.xz"
