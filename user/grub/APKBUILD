# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=grub
pkgver=2.02
pkgrel=6
pkgdesc="Bootloader with support for Linux, Multiboot and more"
url="https://www.gnu.org/software/grub/"
arch="all !s390x"
# strip handled by grub Makefiles, abuild strip breaks xen pv-grub
options="!check !strip"  # Cannot test boot loader.
license="GPL-3.0+"
depends=""
makedepends="bison flex freetype-dev linux-headers lvm2-dev python3 xz unifont
	automake autoconf libtool"
subpackages="$pkgname-dev $pkgname-doc"

# currently grub only builds on x86*, aarch64 and ppc* systems
flavors=""
case "$CARCH" in
x86 | pmmx)	flavors="efi bios";;
x86_64)		flavors="efi bios xenhost";;
aarch64|arm*)	flavors="efi";;
ppc*)		flavors="ieee1275"; makedepends="$makedepends powerpc-utils" ;;
s390x)		flavors="emu" ;;
esac
for f in $flavors; do
	subpackages="$subpackages $pkgname-$f"
done

source="https://ftp.gnu.org/gnu/grub/grub-$pkgver.tar.xz
	fix-gcc-no-pie-specs.patch
	grub2-accept-empty-module.patch
	grub-xen-host_grub.cfg
	the-arch-everyone-uses-and-nobody-loves.patch
	x86_64_asm.patch
	default-grub
	"

prepare() {
	default_prepare
	autoreconf -vif
}

_build_flavor() {
	local flavor="$1"
	shift
	local _configure="$@"
	case $CTARGET_ARCH in
		ppc64) export CFLAGS="-O2 -ggdb -mcpu=970";;  # Workaround for http://savannah.gnu.org/bugs/?52629
	esac

	msg "Building grub for platform $flavor"
	mkdir -p "$srcdir"/build-$flavor
	cd "$srcdir"/build-$flavor
	$builddir/configure \
		--build=$CBUILD \
		--host=$CHOST \
		--target=$CTARGET \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-nls \
		--disable-werror \
		$_configure
	make
}

build() {
	local f
	for f in $flavors; do
		case "$f" in
		bios)		_build_flavor $f --with-platform=pc;;
		efi)		_build_flavor $f --with-platform=efi --disable-efiemu;;
		xenhost)	_build_flavor $f --with-platform=xen;;
		*)		_build_flavor $f --with-platform=$f;;
		esac
	done
}

_install_xen() {
	grub_dir=`mktemp -d`
	cfg=`mktemp`
	grub_memdisk=`mktemp`

	mkdir -p $grub_dir/boot/grub
	echo 'normal (memdisk)/grub.cfg' > $cfg
	sed -e "s/@@PVBOOT_ARCH@@/$CARCH/g" \
		$srcdir/grub-xen-host_grub.cfg \
			> $grub_dir/grub.cfg
	tar -cf - -C $grub_dir grub.cfg > $grub_memdisk

	./grub-mkimage \
		-O $CARCH-xen \
		-c $cfg \
		-d ./grub-core ./grub-core/*.mod \
		-m $grub_memdisk \
		-o $pkgdir/grub-$CARCH-xen.bin

	rm -r "$grub_dir"
	rm "$cfg" "$grub_memdisk"
}

_install_flavor() {
	local flavor="$1"
	cd "$srcdir"/build-$flavor
	case $flavor in
	xenhost)	_install_xen;;
	*)	 	make DESTDIR="$pkgdir" install-strip;;
	esac
}

package() {
	# install BIOS & EFI version into the same directory
	# and overwrite similar files.
	for f in $flavors; do
		_install_flavor $f
	done

	rm -f "$pkgdir"/usr/lib/charset.alias
	install -D -m644 "$srcdir"/default-grub "$pkgdir"/etc/default/grub
	# remove grub-install warning of missing directory
	mkdir -p "$pkgdir"/usr/share/locale
}

bios() {
	pkgdesc="$pkgdesc (BIOS version)"
	depends="$pkgname"
	mkdir -p $subpkgdir/usr/lib/grub
	mv $pkgdir/usr/lib/grub/*-pc $subpkgdir/usr/lib/grub/
}

efi() {
	pkgdesc="$pkgdesc (EFI version)"
	depends="$pkgname efibootmgr efivar"
	mkdir -p $subpkgdir/usr/lib/grub
	mv $pkgdir/usr/lib/grub/*-efi $subpkgdir/usr/lib/grub/
}

xenhost() {
	pkgdesc="$pkgdesc (XEN host version)"
	mkdir -p $subpkgdir/usr/lib/grub-xen
	mv $pkgdir/*-xen.bin $subpkgdir/usr/lib/grub-xen/
}

ieee1275() {
	pkgdesc="$pkgdesc (IEEE1275 version)"
	depends="$pkgname powerpc-utils"
	mkdir -p $subpkgdir/usr/lib/grub
	mv $pkgdir/usr/lib/grub/*-ieee1275 $subpkgdir/usr/lib/grub/
}

emu() {
	pkgdesc="$pkgdesc (EMU version)"
	depends="$pkgname"
	mkdir -p $subpkgdir/usr/lib/grub
	mv $pkgdir/usr/lib/grub/*-emu $subpkgdir/usr/lib/grub/
}

sha512sums="cc6eb0a42b5c8df2f671cc128ff725afb3ff1f8832a196022e433cf0d3b75decfca2316d0aa5fabea75747d55e88f3d021dd93508563f8ca80fd7b9e7fe1f088  grub-2.02.tar.xz
f2a7d9ab6c445f4e402e790db56378cecd6631b5c367451aa6ce5c01cd95b95c83c3dd24d6d4b857f8f42601eba82c855607513eb6ce5b2af6bd6c71f046e288  fix-gcc-no-pie-specs.patch
098a1742aef131c85d63b934a9815879b991f2e73030cb90ac4b5dcd07d249fa0dd0a281e52ada0e10f05d59223493bd416eb47543242bf0ba336a0ebc9b2a1a  grub2-accept-empty-module.patch
4e7394e0fff6772c89683039ccf81099ebbfe4f498e6df408977a1488fd59389b6e19afdbf0860ec271e2b2aea0df7216243dcc8235d1ca3af0e7f4d0a9d60a4  grub-xen-host_grub.cfg
088455205f2f397d60e43eab19ed73994880ea1f442661f7975846cceaf2b112d92fd1341119d7dbfad3af2174dfd4d4721f31dead1ac35f4a3cb7c0d92f8a04  the-arch-everyone-uses-and-nobody-loves.patch
271fdcfbdec567802b351e0a6c38a227ff4ad4eb17533cfa433398fa0929645671a2bfebcc7911d71c1fc3ced94985de148c153834bd2d62018f033eedfbdaf7  x86_64_asm.patch
048d061ac0aab0106f59a3d257739ff5de6c7dc08a4dc9b8b12e9bd2b1ec11f9bc6214013f3d1083b11c3ce41185fcbb5615beb2f290380abf392bb4c3f0d509  default-grub"
