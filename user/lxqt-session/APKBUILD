# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lxqt-session
pkgver=0.14.1
pkgrel=0
pkgdesc="Session management utilities for LXQt"
url="https://lxqt.org"
arch="all"
license="LGPL-2.1+"
depends="xdg-user-dirs"
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.6.0 eudev-dev
	liblxqt-dev>=${pkgver%.*}.0 qt5-qttools-dev kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxde/lxqt-session/releases/download/$pkgver/lxqt-session-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
	rm "$pkgdir"/usr/bin/startlxqt
	rm "$pkgdir"/usr/share/man/man1/start*
}

sha512sums="9274564f3eabe33744bbe8555b1177d8ff6f241f5849bef29f0bb344f506e590e1deab87bf2fdeb867872ebaa0262cda0400f16179951fd66fbcb4d9f7358c4c  lxqt-session-0.14.1.tar.xz"
