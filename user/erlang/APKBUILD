# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=erlang
pkgver=22.0
pkgrel=0
pkgdesc="Soft real-time system programming language"
url="https://www.erlang.org/"
arch="all"
license="Apache-2.0"
depends=""
makedepends="flex libxml2-utils libxslt-dev m4 ncurses-dev openssl-dev perl
	unixodbc-dev"
subpackages="$pkgname-dev"
source="http://erlang.org/download/otp_src_$pkgver.tar.gz
	fix-wx-linking.patch
	safe-signal-handling.patch
	"
builddir="$srcdir/otp_src_$pkgver"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-shared-zlib \
		--enable-ssl=dynamic-ssl-lib \
		--enable-threads
	make
}

check() {
	local _header

	cd "$builddir"
	export ERL_TOP=$builddir

	make release_tests

	for _header in erl_fixed_size_int_types.h \
		${CHOST}/erl_int_sizes_config.h \
		erl_memory_trace_parser.h; do
		cp erts/include/$_header erts/emulator/beam/
	done
	cd release/tests/test_server
	$ERL_TOP/bin/erl -s ts install -s ts smoke_test batch -s init stop
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="3f98697b59b33910fa461141fc4fe9635c4b6a53900dd90920add709dc10468aad7b9fde12419f05b27e038aee5a254319b1c5dc0e26ceabf29a7eb8020b5d23  otp_src_22.0.tar.gz
5f1b05d8be71d5e3d7e8c5ad019329af8f68174251b5b6e0a9ee9cb3da51a10983b8696e23b3954c19de5d54783ec16f38c80c74724341dbafb22fcac83c77d4  fix-wx-linking.patch
dc2fe08e40c73b48b356382c43c982f9f0091e601bbdf6e032358bd5c74c3573b423ef4df454b87c8534105fdbc19ce2245609cc7d5679109c15abaf56d3ef69  safe-signal-handling.patch"
