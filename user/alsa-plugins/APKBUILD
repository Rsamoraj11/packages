# Contributor: Taner Tas <taner76@gmail.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=alsa-plugins
pkgver=1.1.9
pkgrel=0
pkgdesc="Advanced Linux Sound Architecture (ALSA) plugins"
url="https://www.alsa-project.org/main/index.php/Main_Page"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+ AND LGPL-2.1+ AND BSD-3-Clause AND MIT"
makedepends="linux-headers alsa-lib-dev speexdsp-dev ffmpeg-dev
	libsamplerate-dev pulseaudio-dev"
subpackages="$pkgname-lavcrate $pkgname-pulse $pkgname-a52"
source="ftp://ftp.alsa-project.org/pub/plugins/$pkgname-$pkgver.tar.bz2"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

package() {
	make DESTDIR="$pkgdir" install
	find "$pkgdir" -name '*.la' -type f -delete
	rm "$pkgdir"/etc/alsa/conf.d/*.conf
}

lavcrate() {
	pkgdesc="FFmpeg samplerate conversion plugins for ALSA"
	mkdir -p "$subpkgdir"/usr/lib/alsa-lib/ \
		"$subpkgdir"/usr/share/alsa/alsa.conf.d/
	mv "$pkgdir"/usr/lib/alsa-lib/*lavrate* "$subpkgdir"/usr/lib/alsa-lib/
	mv "$pkgdir"/usr/share/alsa/alsa.conf.d/*rate-lav* \
		"$subpkgdir"/usr/share/alsa/alsa.conf.d/
}

pulse() {
	pkgdesc="PulseAudio support for ALSA-only applications"
	mkdir -p "$subpkgdir"/usr/lib/alsa-lib/ \
		"$subpkgdir"/usr/share/alsa/alsa.conf.d/
	mv "$pkgdir"/usr/lib/alsa-lib/*pulse.so "$subpkgdir"/usr/lib/alsa-lib/
	mv "$pkgdir"/usr/share/alsa/alsa.conf.d/*pulse* \
		"$subpkgdir"/usr/share/alsa/alsa.conf.d/
	mv "$pkgdir"/etc/alsa/conf.d/*pulse* \
		"$subpkgdir"/usr/share/alsa/alsa.conf.d/
}

a52() {
	pkgdesc="S16 linear sound format to A52 compressed format for SPDIF output"
	mkdir -p "$subpkgdir"/usr/lib/alsa-lib/ \
		"$subpkgdir"/usr/share/alsa/alsa.conf.d/
	mv "$pkgdir"/usr/lib/alsa-lib/*a52.so "$subpkgdir"/usr/lib/alsa-lib/
	mv "$pkgdir"/usr/share/alsa/alsa.conf.d/*a52* \
		"$subpkgdir"/usr/share/alsa/alsa.conf.d/
}

sha512sums="1969dfe8f2bb4283e81a18da73d2f7f86d8788d1a8764d7195b4830b2f4704c208eacb50a1e26e20c9fe57e7d5bb06a004112d84cc86b4682b4180d2fec4d5d9  alsa-plugins-1.1.9.tar.bz2"
