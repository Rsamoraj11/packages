# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lximage-qt
pkgver=0.14.1
pkgrel=0
pkgdesc="Image viewer and screenshot tool for LXQt"
url="https://lxqt.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=0.6.0 libexif-dev
	libfm-qt-dev>=${pkgver%.*}.0 qt5-qtx11extras-dev qt5-qttools-dev qt5-qtsvg-dev
	kwindowsystem-dev"
source="https://github.com/lxqt/lximage-qt/releases/download/$pkgver/lximage-qt-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="61286ade177a41d954e7b624b9f1320dc64fe6ef5ccc5ffbde5edee1e8cb00f3691b6a04f8eac4314b3fb622ba0613eb4d1851358f3a8c12e4a69d990c1ce3cd  lximage-qt-0.14.1.tar.xz"
