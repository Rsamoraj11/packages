# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=netqmail
pkgver=1.06
pkgrel=5
pkgdesc="The qmail mail transfer agent (community version)"
url="http://www.qmail.org/$pkgname/"
arch="all"
license="Public-Domain"
provides=sendmail
options="suid !check" # suid programs (qmail-queue); no test suite
depends="execline s6 s6-networking"
makedepends="utmps-dev"
subpackages="$pkgname-doc $pkgname-openrc"
install="$pkgname.post-install $pkgname.pre-deinstall"
source="http://www.qmail.org/$pkgname-$pkgver.tar.gz
	0001-DESTDIR.patch
	0002-qbiffutmpx-20170820.patch
	0003-qmailremote-20170716.patch
	0004-notifyfd.patch
	qmail.run
	smtpd.run
	smtpsd.run
	$pkgname.confd
	$pkgname.initd"

makeservicedir()
{
	mkdir -p -m 0755 "$1"/log "$1"/env
	{
	  echo '#!/bin/execlineb -P'
	  echo
	  echo 's6-setuidgid qmaill'
	  echo 's6-envdir ../env'
	  echo 'importas -u IP IP'
	  echo 'exec -c'
	  echo "s6-log t $4"
	} > "$1"/log/run
	echo "$2" > "$1"/notification-fd
	cp -f "$3" "$1"/run
	
	mkdir -p -m 3730 "$1"/event
	mkdir -p -m 0700 "$1"/supervise
	touch "$1"/supervise/lock "$1"/supervise/death_tally
	mkfifo -m 0600 "$1"/supervise/control
	dd if=/dev/zero of="$1"/supervise/status bs=35 count=1
	if test $5 -eq 0 ; then
	  echo /var/qmail/bin:/usr/bin:/usr/sbin:/bin:/sbin > "$1"/env/PATH
	  echo "$2" > "$1"/env/QMAIL_NOTIFY_FD
	else
	  echo 110 > "$1"/env/UID
	  echo 200 > "$1"/env/GID
	  echo > "$1"/env/GIDLIST
	  if test $5 -eq 6 ; then
	    mkdir -p -m 0755 "$1"/data/rules/ip6/::_0
	    touch "$1"/data/rules/ip6/::_0/allow
	    sed -i -e 's/s6-tcpserver /s6-tcpserver6 /' "$1"/run
	  elif test $5 -eq 4 ; then
	    mkdir -p -m 0755 "$1"/data/rules/ip4/0.0.0.0_0
	    touch "$1"/data/rules/ip4/0.0.0.0_0/allow
	    sed -i -e 's/s6-tcpserver /s6-tcpserver4 /' "$1"/run
	  fi
	fi
	chmod 0755 "$1"/run "$1"/log/run
}

build() {
        cd "$builddir"
        echo "$CC $CFLAGS" > conf-cc
        echo "$CC $LDFLAGS -s -static" > conf-ld
        echo "$CC $LDFLAGS" > conf-ldi  # because fakeroot doesn't work with static programs
        echo 022 > conf-patrn
        echo /var/qmail > conf-qmail
        echo 255 > conf-spawn
        { echo alias; echo qmaild; echo qmaill; echo root; echo qmailp; echo qmailq; echo qmailr; echo qmails; } > conf-users
        { echo qmail; echo nofiles; } > conf-groups
        make
}

package() {
	cd "$builddir"
	mkdir -p -m 0755 "$pkgdir"/var/qmail/services "$pkgdir"/var/log/qmail "$pkgdir"/usr/bin "$pkgdir"/usr/sbin "$pkgdir"/usr/share/doc "$pkgdir"/etc/qmail/services "$pkgdir"/etc/conf.d "$pkgdir"/etc/init.d
	chown qmaill:qmaill "$pkgdir"/var/log/qmail
	chmod 2700 "$pkgdir"/var/log/qmail
	cp -f "$srcdir/$pkgname".confd "$pkgdir/etc/conf.d/$pkgname"
	cp -f "$srcdir/$pkgname".initd "$pkgdir/etc/init.d/$pkgname"
	chmod 0755 "$pkgdir/etc/init.d/$pkgname"

	env DESTDIR="$pkgdir" make setup install
	ln -s ../../var/qmail/bin/sendmail "$pkgdir"/usr/sbin/
	ln -s ../../var/qmail/control "$pkgdir"/etc/qmail/control
	rm -rf "$pkgdir"/var/qmail/boot "$pkgdir"/var/qmail/man/cat?
	rm -f "$pkgdir"/var/qmail/bin/qbiff "$pkgdir"/var/qmail/man/man1/qbiff.*
	mv -f "$pkgdir"/var/qmail/man "$pkgdir"/usr/share/man
	mv -f "$pkgdir"/var/qmail/doc "$pkgdir/usr/share/doc/$pkgname-$pkgver"
	echo 255 > "$pkgdir"/var/qmail/control/concurrencylocal
	echo 255 > "$pkgdir"/var/qmail/control/concurrencyremote
	makeservicedir "$pkgdir"/var/qmail/services/qmail 7 "$srcdir"/qmail.run 'n20 s1000000 /var/log/qmail' 0
	makeservicedir "$pkgdir"/etc/qmail/services/smtpd4-skeleton 3 "$srcdir"/smtpd.run '/var/log/smtpd-$IP' 4
	makeservicedir "$pkgdir"/etc/qmail/services/smtpd6-skeleton 3 "$srcdir"/smtpd.run '/var/log/smtpd-$IP' 6
	makeservicedir "$pkgdir"/etc/qmail/services/smtpsd4-skeleton 3 "$srcdir"/smtpsd.run '/var/log/smtpsd-$IP' 4
	makeservicedir "$pkgdir"/etc/qmail/services/smtpsd6-skeleton 3 "$srcdir"/smtpsd.run '/var/log/smtpsd-$IP' 6
}

sha512sums="de40a6d8fac502bd785010434d99b99f2c0524e10aea3d0f2a0d35c70fce91e991eb1fb8f20a1276eb56d7e73130ea5e2c178f6075d138af47b28d9ca6e6046b  netqmail-1.06.tar.gz
ad126cad5c0d35351919ad87022fd94b910519d91cf82f38c158f423bbfc1b82455844a791ba0c69d347af1a20a86b095bed571f75365a86ea786cbc9c626487  0001-DESTDIR.patch
b3af9c29e6d46daa2a1b9f677c6f32892d5f8c9b8d5c2bdd6f34b106dd5ad41394c05a5ebe145c6e29b4ced4482f08b2d09e7818fd309123c0d087600500e336  0002-qbiffutmpx-20170820.patch
cbebdc72c7cc5c437531c9277534ae552c6d044a83b36e3f3ce60ab5563c55eb814d6c543cc0997abab73075d1b517cc0929dd65674d468d517b0ca38196e2b4  0003-qmailremote-20170716.patch
b32a8a36c8ab8872abd4f1a117482f064a6d631a6bb2ba75cafe61743bef09f923d26935d9514eec33a7dec5aeb3d0b517d677e55924859d2db5233bc11f9f11  0004-notifyfd.patch
954a905bac5e3bc49f180dc0de7f6ee4c4ae8f94dd400ee4b06d3c944f1ff1cfc44bddccb07ae439f2523ad06fcb89023e57d091737da88f836013757794e931  qmail.run
c0cd244af4d8186305c51b0e93960bdb1ea6ce40f1adf20c4f72419aa7498e35649590919ebd16547a0313676bf9171c9efea2ff8ac3a5c773b18473a972a977  smtpd.run
719c4ce5ad93cddeafbb734cffeec3fd959d3f374e44e1f34e9a25d638303dd97df41642d3df5c7a069a8db47d1e31c32a16ecd2d04b72860c4e00bbba0c9fcf  smtpsd.run
80ee7e8b3c1ca7cdb00044e6fdd5b9c6a39fd9c882b470f4968c79b974c95e48946a1a3a8b79d9d0ed134ecf09b1185823bf6d022f8b17168e34c18f44ddd16f  netqmail.confd
7600285e70511447b11161a2fc1ca11debc5adcde1d76583d4c715e9710cab876f8be158fd8e034d480588d3d4978c42a9254c29f2db70913105ab009cab6479  netqmail.initd"
