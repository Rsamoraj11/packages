# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=symboleditor
_realpkg=SymbolEditor
pkgver=2.0.0
pkgrel=0
pkgdesc="Symbol library creator for Qt 5"
url="https://userbase.kde.org/SymbolEditor"
arch="all"
license="GPL-2.0+"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdoctools-dev kconfig-dev
	ki18n-dev kio-dev kwidgetsaddons-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/$pkgname/$pkgver/src/SymbolEditor-$pkgver.tar.bz2"
builddir="$srcdir/$_realpkg-$pkgver"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="8baee88fd8ff5f2b6334e80fe7c3fe8044ae521e5a8ffa2588f37dc4a8bf0495c902789bfaaed21a6eaf3c4f6380d18550aaed8046e84b396317104c8a49c993  SymbolEditor-2.0.0.tar.bz2"
