# Contributor: Mari Hahn <mari.hahn@wwu.de>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=tint2
pkgver=16.6.1
pkgrel=0
pkgdesc="Simple, unintrusive panel/taskbar"
url="https://gitlab.com/o9000/tint2"
arch="all"
license="GPL-2.0-only AND GPL-2.0+ AND MIT AND Custom:zlib-optional-acknowledgement"
options="!check"  # No test suite.
depends=""
makedepends="cmake imlib2-dev glib-dev pango-dev cairo-dev
	libxcomposite-dev libxdamage-dev libxinerama-dev libxrandr-dev
	gtk+2.0-dev librsvg-dev startup-notification-dev linux-headers"
subpackages="$pkgname-doc $pkgname-lang"
source="$pkgname-$pkgver.tar.bz2::https://gitlab.com/o9000/$pkgname/repository/v$pkgver/archive.tar.bz2"

prepare() {
	mv "$srcdir/$pkgname-v$pkgver"-* "$builddir"
	mkdir "$builddir"/build
}

build() {
	cd "$builddir"/build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		..
	make
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="25eb3d96f68da1d09711c2f3741e964c880ba911efe3b90b7ebdec4e4734eb7b76d8a84a34431c6a55e785bbbd7ebc8ef447beef2bb57e70f728d28b78b3a81d  tint2-16.6.1.tar.bz2"
