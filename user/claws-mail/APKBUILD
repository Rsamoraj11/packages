# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=claws-mail
pkgver=3.17.3
pkgrel=0
pkgdesc="User-friendly, lightweight, and fast email client"
url="https://www.claws-mail.org/"
arch="all"
license="GPL-3.0-only"
depends="compface"
makedepends="compface-dev curl-dev dbus-glib-dev enchant-dev gnutls-dev
	gpgme-dev gtk+2.0-dev libcanberra-gtk2 libcanberra-dev libetpan-dev
	libical-dev libnotify-dev librsvg-dev openldap-dev
	startup-notification-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://www.claws-mail.org/download.php?file=releases/claws-mail-$pkgver.tar.xz"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-perl-plugin \
		--disable-static
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="b562d785eaedbdec408c4a3db8f4d9326183266fe314509a189c9e1220f15b4f41ccd9d8c58c5194c0267842e8efe900e88eb17c0d17d6069e2543870efa5ef8  claws-mail-3.17.3.tar.xz"
