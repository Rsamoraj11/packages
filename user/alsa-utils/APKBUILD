# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=alsa-utils
pkgver=1.1.9
pkgrel=0
pkgdesc="Advanced Linux Sound Architecture (ALSA) utilities"
url="https://www.alsa-project.org/wiki/Main_Page"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+ AND LGPL-2.0+ AND ISC AND GPL-2.0-only"
depends="bash dialog"
makedepends="alsa-lib-dev fftw-dev ncurses-dev pciutils-dev"
subpackages="$pkgname-doc $pkgname-dbg $pkgname-lang $pkgname-openrc"
replaces="alsaconf"
source="ftp://ftp.alsa-project.org/pub/utils/$pkgname-$pkgver.tar.bz2
	alsaconf.patch
	alsa.initd
	alsa.confd
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-xmlto
	make
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm755 ../alsa.initd "$pkgdir"/etc/init.d/alsa
	install -Dm644 ../alsa.confd "$pkgdir"/etc/conf.d/alsa
}

sha512sums="92fa689ea5897150972d5376e7999ff060cad09cb0b06991d81c87b61a243ecec944e2a4c7ad38878596cd8b4246e44c5a3a35e5bc6452c02ebf35c9bed91970  alsa-utils-1.1.9.tar.bz2
817215be6e9f103a8a187df5b1142c4d2e952f547a64579a9b8cfa58bd762d6a55bde75c0f66f018c0597744d07ccdb08216f7b368db464e36667cecedcc00f3  alsaconf.patch
8a52e6363d5dcfb16e1e607e1af3e46a2148989689e5ab04caf05f84dc68b34cc003b1cf0945ce2c3670cc7f3ef0e40824f689f1efa2d09177c82b13571a7168  alsa.initd
6e716e6230fd3d2c33e3cb2dbf572d632c9ac6452c1768388bea7d3ca22f7c72cf6bcd702580f45cb9089983582011c8b04cbdb4420d14fb988167b1391ea547  alsa.confd"
