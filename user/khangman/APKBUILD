# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=khangman
pkgver=19.04.2
pkgrel=0
pkgdesc="Hangman word game"
url="https://www.kde.org/applications/education/khangman/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev ki18n-dev kcrash-dev kcompletion-dev kconfig-dev kio-dev
	kcoreaddons-dev kconfigwidgets-dev kdeclarative-dev kdoctools-dev
	knewstuff-dev knotifications-dev kxmlgui-dev libkeduvocdocument-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/khangman-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="df333255be6b4e279cd57f461aaa459bb780d631620211594a5364d16759f1a1b171f7538b5e3ec1f8ff9b261d7922fe22584e3270175298c4adabc72b14c313  khangman-19.04.2.tar.xz"
