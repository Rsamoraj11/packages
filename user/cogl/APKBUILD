# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=cogl
pkgver=1.22.4
pkgrel=0
pkgdesc="GObject-based GL/GLES abstraction library"
url="https://gnome.org"
arch="all"
options="!check" # broken testsuite, no logs/messages to determine why
license="MIT AND SGI-B-2.0 AND BSD-3-Clause AND Public-Domain AND LGPL-2.0+ AND Apache-2.0"
makedepends="glib-dev libxcomposite-dev libxrandr-dev mesa-dev cairo-dev
	pango-dev vala gobject-introspection-dev gdk-pixbuf-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.gnome.org/sources/cogl/1.22/cogl-$pkgver.tar.xz"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-kms-egl-platform=yes \
		--enable-xlib-egl-platform=yes
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="85f3af49c16dd2e545a3b91c076ac10107a4b9d0dc785cefe489e91eabdd82837f732685f1f0dca1695fc2f8095f42d5f30f145b659eb4295964787f06c1e37a  cogl-1.22.4.tar.xz"
