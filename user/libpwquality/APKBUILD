# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libpwquality
pkgver=1.4.0
pkgrel=0
pkgdesc="Password quality checking library"
url=" "
arch="all"
license="BSD-3-Clause OR GPL-2.0+"
depends=""
makedepends="cracklib-dev linux-pam-dev python3-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang py3-pwquality:py3"
source="https://github.com/libpwquality/libpwquality/releases/download/libpwquality-$pkgver/libpwquality-$pkgver.tar.bz2"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-pam \
		--with-python-binary=python3
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

py3() {
	pkgdesc="$pkgdesc (Python 3 bindings)"
	depends="python3"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/python3* "$subpkgdir"/usr/lib/
}

sha512sums="b8049f8b71bbfd4d345dbd4c4cffd29e9029b0fca4c95527af54d11a3b06e4708236b630df6c66738368298679c96cb3bf26b1b5d95cb3c5f7e1073cab8a98d9  libpwquality-1.4.0.tar.bz2"
