# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi-mime
pkgver=18.12.3
pkgrel=1
pkgdesc="Libraries to implement basic MIME message handling"
url="https://www.kde.org/"
arch="all"
options="!check"  # regression?
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev akonadi-dev kio-dev kmime-dev"
makedepends="$depends_dev cmake extra-cmake-modules libxslt-dev kdbusaddons-dev
	kconfig-dev kitemmodels-dev kxmlgui-dev shared-mime-info"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/akonadi-mime-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="ecda52f1d41c7b7925b9e71f55f2dca14f38308185a9bdd528f53b109758f79a653b52a2bbb91c138dfc432c798fa9d83307c8652b14269e5d6969a38130ee13  akonadi-mime-18.12.3.tar.xz"
