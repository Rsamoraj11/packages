# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: Adélie Perl Team <adelie-perl@lists.adelielinux.org>
pkgname=perl-moose
pkgver=2.2011
pkgrel=0
pkgdesc="A postmodern object system for Perl 5"
url="https://metacpan.org/release/Moose"
arch="all"
license="Artistic-1.0-Perl"
depends="perl-class-load perl-class-load-xs perl-data-optlist
	perl-devel-globaldestruction perl-devel-overloadinfo
	perl-devel-stacktrace perl-eval-closure perl-module-runtime
	perl-module-runtime-conflicts perl-mro-compat
	perl-package-deprecationmanager perl-params-util perl-sub-exporter
	perl-sub-identify perl-sub-name perl-try-tiny"
makedepends="perl-dev"
checkdepends="perl-cpan-meta-check perl-dist-checkconflicts
	perl-test-cleannamespaces perl-test-fatal perl-test-requires"
subpackages="$pkgname-doc"
source="https://cpan.metacpan.org/authors/id/E/ET/ETHER/Moose-$pkgver.tar.gz"
builddir="$srcdir/Moose-$pkgver"

build() {
	cd "$builddir"
	PERL_MM_USE_DEFAULT=1 perl Makefile.PL INSTALLDIRS=vendor
	make
}

check() {
	cd "$builddir"
	make test
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	find "$pkgdir" \( -name perllocal.pod -o -name .packlist \) -delete
}

sha512sums="f94ed23c45a860b9afa8defe7f5067a65fdd9d457d94a85e4ea192640d09ad73bc336a2e54c4c663643ec0efa32872f4b2aa69083e99093e420cecf5430f34a3  Moose-2.2011.tar.gz"
