# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qt5-qtbase
_pkgname=qtbase-opensource-src
pkgver=5.9.7
pkgrel=0
pkgdesc="Qt 5.9 base components"
url="https://www.qt.io/"
arch="all"
license="LGPL-2.0 with exceptions OR GPL-3.0 with exceptions"
_sub="$pkgname-sqlite $pkgname-odbc $pkgname-postgresql
	$pkgname-tds $pkgname-x11"
depends_dev="dbus-dev fontconfig-dev freetype-dev glib-dev libice-dev
	libpng-dev libsm-dev libx11-dev libxext-dev mesa-dev openssl-dev perl
	sqlite-dev zlib-dev $_sub"
makedepends="$depends_dev
	at-spi2-core-dev bison cups-dev eudev-dev flex freetds-dev freetype-dev
	gperf gtk+2.0-dev harfbuzz-dev hicolor-icon-theme icu-dev
	libexecinfo-dev libinput-dev libjpeg-turbo-dev libxkbcommon-dev
	libxi-dev libxrandr-dev libxrender-dev libxslt-dev libxv-dev mtdev-dev
	pcre2-dev postgresql-dev unixodbc-dev xcb-util-dev xcb-util-image-dev
	xcb-util-keysyms-dev xcb-util-wm-dev xcb-util-renderutil-dev cmd:which
	"
subpackages="$pkgname-dev $pkgname-doc $_sub"
source="https://download.qt.io/official_releases/qt/${pkgver%.*}/$pkgver/submodules/$_pkgname-$pkgver.tar.xz
	link-to-execinfo.patch
	qt-musl-iconv-no-bom.patch
	"

_qt5_prefix=/usr/lib/qt5
_qt5_datadir=/usr/share/qt5
builddir="$srcdir"/$_pkgname-$pkgver

prepare() {
	default_prepare
	cd "$builddir"
	sed -i -e "s|-O2|$CXXFLAGS|" \
		-e "/^QMAKE_RPATH/s| -Wl,-rpath,||g" \
		-e "/^QMAKE_LFLAGS\s/s|+=|+= $LDFLAGS|g" \
		mkspecs/common/linux.conf
}

build() {
	cd "$builddir"

	local ARCH_OPTS=""
	[ ${CTARGET_ARCH} != "pmmx" ] || ARCH_OPTS="-no-sse2"

	./configure -confirm-license -opensource \
		-archdatadir "$_qt5_prefix" \
		-bindir "$_qt5_prefix"/bin \
		-datadir "$_qt5_datadir" \
		-dbus-linked \
		-docdir /usr/share/doc/qt5 \
		-examplesdir /usr/share/doc/qt5/examples \
		-glib \
		-icu \
		-importdir "$_qt5_prefix"/imports \
		-libexecdir "$_qt5_prefix"/libexec \
		-no-pch \
		-no-rpath \
		-nomake examples \
		-opengl \
		-openssl-linked \
		-optimized-qmake \
		-plugin-sql-odbc \
		-plugin-sql-psql \
		-plugin-sql-sqlite \
		-plugin-sql-tds \
		-plugindir "$_qt5_prefix"/plugins \
		-prefix /usr \
		-silent \
		-sysconfdir /etc/xdg \
		-system-freetype \
		-system-harfbuzz \
		-system-libjpeg \
		-system-libpng \
		-system-pcre \
		-system-sqlite \
		-system-xcb \
		-system-zlib \
		-translationdir "$_qt5_datadir"/translations \
		-no-reduce-relocations \
		-debug -optimize-debug -force-debug-info \
		$ARCH_OPTS
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make INSTALL_ROOT="$pkgdir" install
	mkdir -p "$pkgdir"/usr/bin/
	local i
	for i in "$pkgdir"/$_qt5_prefix/bin/*; do
		ln -s ../lib/qt5/bin/"$(basename $i)" \
			"$pkgdir"/usr/bin/"$(basename $i)"
	done
}

_mv_files() {
	local i
	for i; do
		mkdir -p "$subpkgdir"/${i%/*}
		mv "$pkgdir"/$i "$subpkgdir"/$i
	done
}

dev() {
	cd "$pkgdir"
	_mv_files usr/lib/qt5/mkspecs \
		usr/lib/cmake \
		usr/lib/qt5/bin \
		usr/bin \
		$(find usr/ -name '*.prl')
	default_dev
}

sqlite() {
	pkgdesc="SQLite driver for Qt5's SQL classes"
	cd "$pkgdir"
	_mv_files usr/lib/qt5/plugins/sqldrivers/libqsqlite*
}

odbc() {
	pkgdesc="ODBC driver for Qt5's SQL classes"
	cd "$pkgdir"
	_mv_files usr/lib/qt5/plugins/sqldrivers/libqsqlodbc*
}

postgresql() {
	pkgdesc="PostgreSQL driver for Qt5's SQL classes"
	cd "$pkgdir"
	_mv_files usr/lib/qt5/plugins/sqldrivers/libqsqlpsql*
}

mysql() {
	pkgdesc="MySQL driver for Qt5's SQL classes"
	cd "$pkgdir"
	_mv_files usr/lib/qt5/plugins/sqldrivers/libqsqlmysql*
}

tds() {
	pkgdesc="TDS driver for Qt5's SQL classes"
	cd "$pkgdir"
	_mv_files usr/lib/qt5/plugins/sqldrivers/libqsqltds*
}

x11() {
	pkgdesc="Qt5 GUI-related libraries"
	depends="hicolor-icon-theme"
	cd "$pkgdir"
	_mv_files \
		usr/lib/libQt5EglFSDeviceIntegration.so.* \
		usr/lib/libQt5EglFsKmsSupport.so.* \
		usr/lib/libQt5Gui.so.* \
		usr/lib/libQt5OpenGL.so.* \
		usr/lib/libQt5PrintSupport.so.* \
		usr/lib/libQt5Widgets.so.* \
		usr/lib/libQt5XcbQpa.so.* \
		usr/lib/qt5/plugins/egldeviceintegrations \
		usr/lib/qt5/plugins/generic \
		usr/lib/qt5/plugins/image* \
		usr/lib/qt5/plugins/platform* \
		usr/lib/qt5/plugins/printsupport* \
		usr/lib/qt5/plugins/xcbglintegrations

	scanelf -Rn usr/ | egrep '(libX|libQt5Gui|libGL)' && return 1
	return 0
}

sha512sums="7121837501b810f31863fbb3a6ea7189d3e38789ff25eb49b639a69b73913761c8cba18b65046519693d2f5c8755c2197feff1df1ec3d88676d00c649a326e1e  qtbase-opensource-src-5.9.7.tar.xz
ee78a44e28ba5f728914bfc3d8d5b467896c7de11a02d54b0bce11e40a4338b1f776c1fcc30cbd436df4f548c1ab0b4fe801f01b162ddd5c0f892893e227acfd  link-to-execinfo.patch
7d68421a14f0259535c977d8a521c98918193c107b76ac664571b12f5b0d7588a0d0e1297af412a26753a393b21f3f44c3274fa8ab5bc87f03705a3a03acb444  qt-musl-iconv-no-bom.patch"
