# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=calligra
pkgver=3.1.0
pkgrel=2
pkgdesc="KDE Office suite"
url="https://www.calligra.org/"
arch="all"
options="!check"  # Tests require X11
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtscript-dev

	karchive-dev kcmutils-dev kcodecs-dev kcompletion-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kdoctools-dev
	kguiaddons-dev ki18n-dev kiconthemes-dev kio-dev kitemviews-dev
	kjobwidgets-dev kdelibs4support-dev knotifications-dev kparts-dev
	knotifyconfig-dev kross-dev ktextwidgets-dev kwallet-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev sonnet-dev

	kactivities-dev khtml-dev kholidays-dev qt5-qtwebkit-dev

	boost-dev eigen-dev fontconfig-dev freetype-dev gsl-dev lcms2-dev
	libetonyek-dev libgit2-dev libodfgen-dev librevenge-dev libvisio-dev
	libwpd-dev libwpg-dev libwps-dev marble-dev poppler-dev poppler-qt5-dev
	qca-dev

	kcalcore-dev kcontacts-dev kdiagram-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/calligra/$pkgver/calligra-$pkgver.tar.xz
	https://download.kde.org/stable/calligra/$pkgver/calligraplan-$pkgver.tar.xz
	c99-math.patch
	poppler.patch
	poppler2.patch
	poppler3.patch
	poppler4.patch
	poppler-73.patch
	"

prepare() {
	default_prepare

	cd "$builddir"
	mv ../calligraplan-$pkgver plan
	sed -e "/add_subdirectory(plan)/s/#//" \
		-e "/^calligra_disable_product(APP_PLAN/s/^/#/" \
		-i CMakeLists.txt
	mkdir build
}

build() {
	cd "$builddir"/build
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		-DPRODUCTSET="KARBON PLAN SHEETS STAGE WORDS" \
		-DBUILD_UNMAINTAINED=True \
		${CMAKE_CROSSOPTS} \
		..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="ad1b689a5b36deafcf6985d4a0a1e8148d10e19fb4a0be8343c6e1d24146b9a8ffa0890ab5bbb21816b3134d21150b57f6159db1a30cc54c0fde5bf7bdf4537b  calligra-3.1.0.tar.xz
a8ccc6ee2f0381b811926a296122464bdb79aad7c75ff0f7a554e4596165ff7fd7ef62c9af9232ea36542d6b538446c8920fe77cbbe7ffacdf6e6b99a1b68156  calligraplan-3.1.0.tar.xz
b03d2f33a0233638be06b4219328404eac4a77c508619c4037b3b53556b257fc4888cd690bb3755562040a198cbd51a323d13553fed3f2082398556a49482c6e  c99-math.patch
960614eb2d367443810cc356d004d43867a4fa4085b054ffd1522b94ef09e9338e723117c91148383d33d54104729750a199ab1b75569aaa7164a908afd3cd4c  poppler.patch
53296af0b1ad39c523bc93a0040f1316dfb52e2077d277a83841a741c5cb48d81efb1f6328b2af2c8f1dc92db12c4d771a9a65028498d7802b2c30dd702b9455  poppler2.patch
a22b78d9664284ee5de7a4299907ec37a7835cb6ae3af8e7c1ee0202cea8c1e544fbad7395e56115fb5fe13ec06aeff22e381431e20c6c151c93003cf8a45446  poppler3.patch
df0e592f8c314581b1811776f8aa1c3dd32f05f966f23ef4dd023c41dad759aea8b674b32e2bd7bee36efe4ba0ce0bdd08f3fc30d10115330605d09d46883a28  poppler4.patch
8440f1c36aba9cb54efa36fae73d605f57e78d055225c776791b14554d6be6f081f8cdc3fa65fb9c0b600a3b030d7776b45f631b9bc00d495ab16bfc35bd66f4  poppler-73.patch"
