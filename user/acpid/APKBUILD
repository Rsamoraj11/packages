# Maintainer:
# Contributor: Morten Linderud <foxboron@archlinux.org>
# Contributor: Sébastien Luttringer
# Contributor: xduugu
# Contributor: Manolis Tzanidakis
# Contributor: Jonathan Schmidt <j.schmidt@archlinux.us>
# Contributor: multiplexd <multi@in-addr.xyz>

pkgname=acpid
pkgver=2.0.31
pkgrel=0
pkgdesc="Daemon for handling ACPI power management events"
url="https://sourceforge.net/projects/acpid2/"
arch="all"
options="!check" # No test suite.
license="GPL-2.0+ AND GPL-2.0-only"
depends=""
makedepends=""
subpackages="$pkgname-doc $pkgname-openrc"
source="http://downloads.sourceforge.net/sourceforge/acpid2/$pkgname-$pkgver.tar.xz
	handler.sh
	default
	acpid.initd
	acpid.confd"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sbindir=/usr/sbin \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install

	install -m 755 -D "$srcdir"/acpid.initd \
		"$pkgdir"/etc/init.d/acpid
	install -m 644 -D "$srcdir"/acpid.confd \
		"$pkgdir"/etc/conf.d/acpid
	install -m 644 -D "$srcdir"/default \
		"$pkgdir"/etc/acpi/events/default
	install -m 755 -D "$srcdir"/handler.sh \
		"$pkgdir"/etc/acpi/handler.sh
}

sha512sums="05eb96cdae698731b14caa89aa01239a0f16ce732f897d295447753aab7c8d49227c9b9ce901c6fd3bfdb117688e6ed80ec61aea0f64d75c9c0afe2aea2aa91a  acpid-2.0.31.tar.xz
f5935340391a927b2c0b8d36f6b9579d740299a46c210f762483336f2685e80bdfc237dd5ac5eeed5e458eff2fa436cd36e3277c4ee2085e05662e1525c56edd  handler.sh
2ca236168ce6aaa56c980568c781d6e51590870b7a7936c74bf72532ef3f6c60a369f37597202f3a236d60637fd0daa6611d4ae0348484011ff71871a9914246  default
7381d30b5c6478cdbf5dff93ae95baa0b3b1fe0a04b02cf491831f1657d6f71b8eef121b7e78f3201d11a5856bfb30df0a57437c76e6fbe05ad88cd45e86ae64  acpid.initd
518cb397b2aa63b893ead1be08f32fe436d19b72663dee66834cfbc112b003333f0df8b9e4f1ffe64b813783f657d3fe8b9a0c5e992d5665583357e68b266705  acpid.confd"
