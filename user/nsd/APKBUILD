# Contributor: Luis Ressel <aranea@aixah.de>
# Maintainer: Luis Ressel <aranea@aixah.de>
pkgname=nsd
pkgver=4.2.0
pkgrel=0
pkgdesc="An authoritative only name server"
url="https://www.nlnetlabs.nl/projects/nsd/about/"
arch="all"
options="!check" # No test suite
license="BSD-3-Clause"
depends=""
makedepends="libevent-dev openssl-dev"
subpackages="$pkgname-doc $pkgname-openrc"
install="$pkgname.pre-install"
pkgusers="nsd"
pkggroups="nsd"
source="https://nlnetlabs.nl/downloads/$pkgname/$pkgname-$pkgver.tar.gz
	nsd.confd
	nsd.initd"

build() {
	cd "$builddir"

	# dnstap has yet unpackaged dependencies
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--with-pidfile=/run/nsd.pid \
		--disable-dnstap \
		--enable-bind8-stats \
		--enable-ratelimit \
		--enable-ratelimit-default-is-off \
		--enable-recvmmsg \
		--with-ssl=/usr \
		--with-libevent=/usr
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

openrc() {
	default_openrc
	install -Dm755 "$srcdir/nsd.initd" "$subpkgdir/etc/init.d/nsd"
	install -Dm644 "$srcdir/nsd.confd" "$subpkgdir/etc/conf.d/nsd"
}

sha512sums="caa14fcd599ddc631cb74c3a56e571044dae1deb2fa9bd6b062f143954f9207b64b42ab5eab917360161f96bae8711df932f3e18b58be98b3f7b640071e7e807  nsd-4.2.0.tar.gz
f0ef1d3427e92650239d9d91402810c045fc9223e3f42ce86986422bf2039a0bcc02dffdfe1153d54de5c76c8f2bdc3e34fe341c65b41f2d333b02c00b5b0eae  nsd.confd
139e52dec98792173f06d298574db0d0e6966a06af8a0a3069487beb01fd570c09d22322569b54bacdc43232dbfb99a8c497d4417d2bbfee88bcdd9d1b4d22f7  nsd.initd"
