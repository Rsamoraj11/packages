# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=exiv2
pkgver=0.27.1
pkgrel=0
pkgdesc="Exif, IPTC and XMP metadata library and tools"
url="https://www.exiv2.org/"
arch="all"
license="GPL-2.0+"
depends=""
depends_dev="expat-dev zlib-dev"
makedepends="$depends_dev bash cmake"
checkdepends="python3 libxml2 cmd:which"
subpackages="$pkgname-dev $pkgname-doc"
source="http://www.exiv2.org/builds/exiv2-$pkgver-Source.tar.gz"
builddir="$srcdir/$pkgname-$pkgver-Source"

# secfixes:
#   0.26-r2:
#     - CVE-2018-19535
#   0.27.1-r0:
#     - CVE-2017-9239
#     - CVE-2017-9953
#     - CVE-2017-11336
#     - CVE-2017-11337
#     - CVE-2017-11338
#     - CVE-2017-11339
#     - CVE-2017-11340
#     - CVE-2017-11553
#     - CVE-2017-11591
#     - CVE-2017-11592
#     - CVE-2017-11683
#     - CVE-2017-12955
#     - CVE-2017-12956
#     - CVE-2017-12957
#     - CVE-2017-14857
#     - CVE-2017-14858
#     - CVE-2017-14859
#     - CVE-2017-14860
#     - CVE-2017-14861
#     - CVE-2017-14862
#     - CVE-2017-14863
#     - CVE-2017-14864
#     - CVE-2017-14865
#     - CVE-2017-14866
#     - CVE-2017-17669
#     - CVE-2017-17722
#     - CVE-2017-17723
#     - CVE-2017-17724
#     - CVE-2017-17725
#     - CVE-2017-18005
#     - CVE-2017-1000126
#     - CVE-2017-1000127
#     - CVE-2017-1000128
#     - CVE-2018-4868
#     - CVE-2018-5772
#     - CVE-2018-8976
#     - CVE-2018-8977
#     - CVE-2018-9145
#     - CVE-2018-10772
#     - CVE-2018-10780
#     - CVE-2018-10958
#     - CVE-2018-10998
#     - CVE-2018-10999
#     - CVE-2018-11037
#     - CVE-2018-11531
#     - CVE-2018-12264
#     - CVE-2018-12265
#     - CVE-2018-14046
#     - CVE-2018-14338
#     - CVE-2018-16336
#     - CVE-2018-17229
#     - CVE-2018-17230
#     - CVE-2018-17282
#     - CVE-2018-17581
#     - CVE-2018-19107
#     - CVE-2018-19108

prepare() {
	default_prepare
	mkdir build
}

build() {
	cd "$builddir/build"
	cmake -DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo ..
	make
}

check() {
	cd "$builddir/build"
	make tests
}

package() {
	cd "$builddir/build"
	make DESTDIR="$pkgdir" install
}

sha512sums="038b51241f5bfb323eb298695b5397a7d88d5c7d7303828e5e20b3f82c3df2615cee3e7e3426ea17438ca05d5abea10984cfd41f0649ddab72df1d1415bf3529  exiv2-0.27.1-Source.tar.gz"
