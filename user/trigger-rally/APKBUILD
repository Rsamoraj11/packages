# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=trigger-rally
pkgver=0.6.6.1
pkgrel=0
pkgdesc="Fast-paced rally racing game"
url="http://trigger-rally.sourceforge.net/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0-only"
depends=""
makedepends="freealut-dev glew-dev glu-dev openal-soft-dev physfs-dev sdl2-dev
	sdl2_image-dev tinyxml2-dev"
subpackages="$pkgname-doc"
source="https://downloads.sourceforge.net/trigger-rally/trigger-rally-$pkgver.tar.gz
	dont-strip.patch
	trigger-rally.desktop
	"

build() {
	cd "$builddir"/src
	make OPTIMS="$CXXFLAGS -Wno-deprecated-declarations" DESTDIR="$pkgdir" prefix=/usr build
}

package() {
	local _icon=""

	cd "$builddir"/src
	make OPTIMS="$CXXFLAGS" DESTDIR="$pkgdir" prefix=/usr install

	install -D -m644 "$srcdir"/trigger-rally.desktop \
		"$pkgdir"/usr/share/applications/trigger-rally.desktop

	for _icon in 16 22 24 32 36 48 64 72 96 128 192 256; do
		install -D -m644 "$builddir"/data/icon/trigger-$_icon.png \
			"$pkgdir"/usr/share/icons/hicolor/${_icon}x${_icon}/apps/trigger.png
	done
	install -D -m644 "$builddir"/data/icon/trigger-rally-icons.svg \
		"$pkgdir"/usr/share/icons/hicolor/scalable/apps/trigger.svg
}

sha512sums="feed805858ef63907bb10088761f1219b22fe55ead268511ef73b18aa0f18d79e87d4e2cbfb76361b1dec3949b59464af33efe31e81f06ae7e163430f3336669  trigger-rally-0.6.6.1.tar.gz
a81781058177e012daaad58d72e4bdbdbc2730b1df76941ba03f06367545b213ba82a33bab46deba31570973deb1802a3d459704c3a963c38eec69ef82f228d5  dont-strip.patch
ddaf7d89353280f7952d6cffd06182ea28640473e18eff06809c48477349248f57922eaff9bf816261c32f8fdc167d933f345422f9885638041dd9f5e5ac0524  trigger-rally.desktop"
