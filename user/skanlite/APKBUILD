# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=skanlite
pkgver=2.1.0.1
pkgrel=0
pkgdesc="Simple image scanning application"
url="https://www.kde.org/applications/graphics/skanlite/"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libksane-dev kio-dev
	kcoreaddons-dev ki18n-dev kxmlgui-dev kdoctools-dev libpng-dev zlib-dev
	ktextwidgets-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/skanlite/2.1/skanlite-$pkgver.tar.xz
	fix-version.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="8eb1a32994259010d4a134c1b25b74b8cc03ba4fa6ce70ee2026a3070c675c2b11f38770453d197008fbe12df976cc653362eb44d6ef89a7f0173c3a2cef3658  skanlite-2.1.0.1.tar.xz
91ce7a1f9a2796be511f20978fdea9e19c9eb6415ae5d407fbc855675d2e6809ddc85546b4e018ca0d72262d86104292c81e490613ae9045d10257e646da8732  fix-version.patch"
