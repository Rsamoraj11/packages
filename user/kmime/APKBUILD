# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmime
pkgver=19.04.2
pkgrel=0
pkgdesc="KDE support library for MIME"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 kcodecs-dev
	ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kmime-$pkgver.tar.xz
	egregious-versions.patch
	"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# https://bugs.kde.org/show_bug.cgi?id=385479
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(headertest|messagetest|dateformattertest)'
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="043cddd297e31172a80167b872b6dcf675c659ec8613cbef8f41bc84bbf8f499f9b85600be0cd36fc6013ae1facadbf8fe022e107a37699b0f15aaac66b498ba  kmime-19.04.2.tar.xz
b5f04ab4f7ed60bde1eeb50141c5b51a4f285d7b125d834559419d56039c1d540a8d44f508801ae9ac2c6d91c3841d4e64c3856e2c5bfa33eb64175dc9b5c608  egregious-versions.patch"
