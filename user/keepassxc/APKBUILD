# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=keepassxc
pkgver=2.4.1
pkgrel=0
pkgdesc="A community revival of the KeePassX password manager"
url="https://keepassxc.org"
arch="all"
license="(GPL-2.0-only OR GPL-3.0-only) AND MIT AND BSD-4-Clause AND ISC AND (LGPL-2.1-only OR GPL-3.0-only) AND (LGPL-2.1-only OR LGPL-3.0-only) AND CC0-1.0 AND Public-Domain AND LGPL-2.1+ AND LGPL-3.0+"
depends="hicolor-icon-theme"
makedepends="argon2-dev cmake libgcrypt-dev libqrencode-dev
	libxi-dev libxtst-dev qt5-qtbase-dev qt5-qtsvg-dev qt5-qttools-dev
	qt5-qtx11extras-dev xz zlib-dev"
subpackages="$pkgname-doc"
source="https://github.com/keepassxreboot/$pkgname/releases/download/$pkgver/$pkgname-$pkgver-src.tar.xz"

prepare() {
	default_prepare
	mkdir build
}

build() {
	cd "$builddir/build"
	cmake -DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo ..
	make
}

check() {
	cd "$builddir/build"
	make test
}

package() {
	cd "$builddir/build"
	make DESTDIR="$pkgdir" install
}

sha512sums="4369043700d3de4fece0a1dd0f8aab3f621df379bc9d0214b4c70ab72d081f978104aa09fd39c4b4bf8b8df8f78266835dc206c31e7174bdf872a977c0d79502  keepassxc-2.4.1-src.tar.xz"
