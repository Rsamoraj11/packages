# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: 
pkgname=v4l-utils
pkgver=1.16.4
pkgrel=0
pkgdesc="Userspace tools and conversion library for Video 4 Linux"
url="https://www.linuxtv.org/wiki/index.php/V4l-utils"
arch="all"
license="LGPL-2.0+"
makedepends="qt5-qtbase-dev libjpeg-turbo-dev argp-standalone linux-headers
	eudev-dev alsa-lib-dev"
subpackages="$pkgname-dev $pkgname-doc qv4l2 $pkgname-libs ir_keytable"
source="https://www.linuxtv.org/downloads/v4l-utils/$pkgname-$pkgver.tar.bz2
	qv4l2.svg
	qv4l2.desktop
	getsubopt.patch
	"

build() {
	cd "$builddir"
	[ "$CLIBC" = "musl" ] && export CFLAGS="$CFLAGS -D__off_t=off_t"
	LIBS="-largp -lintl" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-libdvbv5 \
		--disable-static
        make
}

check() {
	cd "$builddir"
	make check
}

package() {
        cd "$builddir"
        make -j1 DESTDIR="$pkgdir" install
	install -Dm644 "$srcdir"/qv4l2.desktop \
		"$pkgdir"/usr/share/applications/qv4l2.desktop
	install -Dm644 "$srcdir"/qv4l2.svg \
		"$pkgdir"/usr/share/icons/hicolor/scalable/apps/qv4l2.svg
}

qv4l2() {
	pkgdesc="Qt V4L2 test control and streaming test application"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/qv4l2 "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/share "$subpkgdir"/usr/
}

libs() {
	pkgdesc="Collection of Video4Linux support libraries"
	mkdir -p "$subpkgdir"/usr/lib
        mv "$pkgdir"/usr/lib/* \
                "$subpkgdir"/usr/lib
}

ir_keytable() {
	pkgdesc="Alter keymaps of Remote Controller devices"
	mkdir -p "$subpkgdir"/lib/udev/rc_keymaps \
		"$subpkgdir"/lib/udev/rules.d \
		"$subpkgdir"/usr/bin \
		"$subpkgdir"/etc
	mv "$pkgdir"/lib/udev/rc_keymaps/* \
                "$subpkgdir"/lib/udev/rc_keymaps
	mv "$pkgdir"/lib/udev/rules.d/* \
		"$subpkgdir"/lib/udev/rules.d
	mv "$pkgdir"/usr/bin/ir-keytable \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/etc/rc_maps.cfg \
		"$subpkgdir"/etc
}

sha512sums="451e3af203b43fcf9fc61e00a236c030c932eabc0b749269277fc42da05464a2b74581255e7d9bbbac35b43ece318b640c3fde8d074c71723a3594a27ee982ff  v4l-utils-1.16.4.tar.bz2
bc18280046c15b19984103f7c2bb44a0aea79715803c64f0c64bc932499c09022c956914c3b15ae59499adc09f6fbff5378be45707fe851250f495a26b63d682  qv4l2.svg
6f74aa524b3de420eeb8de788ff3f717020732a3f1f6530caee50e63aae7eddbe5f551ffc50065c9f5d6078c13bace089948ecdcacf01f8b82c1a44960e06315  qv4l2.desktop
9a4f6d0a44d30bb7afe4db8b40074d362f240cae1f1b13feb0eb2b1b666479fc6f73ef27542f6f80fb1b922c9540feadc9ff8563890ff3041d3f7fc62e504e29  getsubopt.patch"
