# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=s6-dns
pkgver=2.3.0.2
pkgrel=0
pkgdesc="skarnet.org's DNS client libraries and command-line DNS client utilities"
url="https://skarnet.org/software/s6-dns/"
arch="all"
options="!check"
license="ISC"
_skalibs_version=2.8
depends=
makedepends="skalibs-dev>=$_skalibs_version skalibs-libs-dev>=$_skalibs_version"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	cd "$builddir"
	./configure \
		--enable-shared \
		--enable-static \
		--disable-allstatic \
		--prefix=/usr \
		--libdir=/usr/lib \
		--libexecdir="/usr/lib/$package" \
		--with-dynlib=/lib
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}


libs() {
        pkgdesc="$pkgdesc (shared libraries)"
        depends="skalibs-libs>=$_skalibs_version"
        mkdir -p "$subpkgdir/usr/lib"
        mv "$pkgdir"/usr/lib/*.so.* "$subpkgdir/usr/lib/"
}


dev() {
        pkgdesc="$pkgdesc (development files)"
        depends="skalibs-dev>=$_skalibs_version"
        mkdir -p "$subpkgdir/usr/include" "$subpkgdir/usr/lib"
        mv "$pkgdir/usr/include" "$subpkgdir/usr/"
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir/usr/lib/"
}


libsdev() {
        pkgdesc="$pkgdesc (development files for dynamic linking)"
        depends="$pkgname-dev"
        mkdir -p "$subpkgdir/usr/lib"
        mv "$pkgdir"/usr/lib/*.so "$subpkgdir/usr/lib/"
}


doc() {
        pkgdesc="$pkgdesc (documentation)"
        depends=
        install_if="docs $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/share/doc"
        cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="fec0edf852423bf8f717fcdc3c00c8f38e7bab4d9a03d14e6e81ad3f175b5db57be44409761bbd990f56f81c3ae8e0aa76ab8c5b65fec823a0ce392bf41cdf33  s6-dns-2.3.0.2.tar.gz"
