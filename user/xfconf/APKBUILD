# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=xfconf
pkgver=4.13.7
pkgrel=1
pkgdesc="Configuration framework for the XFCE desktop environment"
url="https://xfce.org"
arch="all"
options="!check" # tests require X11
license="LGPL-2.1+ AND GPL-2.0+"
depends="dbus"
makedepends="intltool gtk+3.0-dev libxfce4util-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://archive.xfce.org/src/xfce/xfconf/4.13/xfconf-$pkgver.tar.bz2"

build() {
	LIBS="-lintl" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

perl() {
	depends="perl perl-glib"
	pkgdesc="Perl bindings for xfconf"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/perl5 "$subpkgdir"/usr/lib
}

sha512sums="113fb22b449a318b0cca6521914eda6cfeb6b2056a1aad4d49a5e83c28661c843c9912bbf7eddb7d6cd1f172b532c73dd02259bf6fabe60a377024f9f8d5825d  xfconf-4.13.7.tar.bz2"
