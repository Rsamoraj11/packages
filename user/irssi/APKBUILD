# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=irssi
pkgver=1.2.1
pkgrel=0
pkgdesc="Text-based IRC client"
url="https://irssi.org"
arch="all"
license="GPL-2.0+ AND ISC"
makedepends="ncurses-dev glib-dev openssl-dev perl-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-perl"
source="https://github.com/irssi/irssi/releases/download/$pkgver/irssi-$pkgver.tar.xz"

# secfixes: irssi
#   1.2.1-r0:
#     - CVE-2019-13045

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-true-color \
		--with-perl=module \
		--with-perl-lib=vendor
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

perl() {
	depends="perl"
	pkgdesc="Perl support & scripts for irssi"

	mkdir -p "$subpkgdir"/usr "$subpkgdir"/usr/share/irssi
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr
	mv "$pkgdir"/usr/share/irssi/scripts "$subpkgdir"/usr/share/irssi
}
sha512sums="67c4501b5a0055c1b24fa6753305658de809cd66e952e6f9233701a112989fd8721a065b1c681725b82346b40b53a29bd2b6b8b8315ac0ad196235a9e5156d5a  irssi-1.2.1.tar.xz"
