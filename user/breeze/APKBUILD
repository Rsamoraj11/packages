# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=breeze
pkgver=5.12.7
pkgrel=0
pkgdesc="Default KDE Plasma 5 style"
url="https://www.kde.org/"
arch="all"
license="(GPL-2.0-only AND GPL-3.0-only) AND MIT AND GPL-2.0+"
depends=""
depends_dev="extra-cmake-modules"
makedepends="$depends_dev cmake extra-cmake-modules python3 kcmutils-dev
	kconfigwidgets-dev kcoreaddons-dev kdecoration-dev kguiaddons-dev
	ki18n-dev kpackage-dev frameworkintegration-dev kwindowsystem-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/breeze-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="c0e564fa6df64c225ea7754271c138d4b4304068c5fcf2e0502f109af3478f37d2781ac13d068b0818c819c8f60d6c4a333ff8940334c3ee5030607f80080b99  breeze-5.12.7.tar.xz"
