# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=dragonplayer
pkgver=19.04.2
pkgrel=0
pkgdesc="Multimedia player with a focus on simplicity"
url="https://www.kde.org/applications/multimedia/dragonplayer/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfig-dev kcrash-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kdoctools-dev
	ki18n-dev kjobwidgets-dev kio-dev kparts-dev solid-dev phonon-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev knotifications-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/dragon-$pkgver.tar.xz"
builddir="$srcdir"/dragon-$pkgver

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="f46d949e44ae4014fcad9186f36de50068095154af9632fc6d5ba4b7285b078caa4d09e0f1d3d84edb08aace6335db2ec6c8f4771061e2e2932901fd198eca7d  dragon-19.04.2.tar.xz"
