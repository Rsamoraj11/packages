# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kbookmarks
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for managing XBEL-format bookmarks"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires X11.
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev kcoreaddons-dev kcodecs-dev
	kconfigwidgets-dev kiconthemes-dev kxmlgui-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kbookmarks-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make	
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="f7c18ee6a0135ac93bbd4034b2c11b6142404a9b5c5872374ee2dc2d5f0cf70288793df4da4be980c0e1bf757ccbfc6ca8a83c490691e80308ec7133eb49c3ba  kbookmarks-5.54.0.tar.xz"
