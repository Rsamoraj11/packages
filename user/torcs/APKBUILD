# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=torcs
pkgver=1.3.7
pkgrel=0
pkgdesc="The Open Racing Car Simulator"
url="http://torcs.sourceforge.net/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
makedepends="freealut-dev freeglut-dev libogg-dev libpng-dev libsm-dev
	libvorbis-dev libxmu-dev libxrandr-dev libxrender-dev libxt-dev mesa-dev
	openal-soft-dev plib-dev zlib-dev"
subpackages="$pkgname-data::noarch"
source="https://sourceforge.net/projects/torcs/files/all-in-one/$pkgver/torcs-$pkgver.tar.bz2
	torcs-1.3.6-as-needed.patch
	torcs-1.3.6-format.patch
	torcs-1.3.6-gcc6.patch
	torcs-1.3.6-no-automake.patch
	define-uint.patch
	"

build() {
	cd "$builddir"
	export CXXFLAGS="$CXXFLAGS -fpermissive"  # GCC 8
	autoreconf
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make -j1
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

data() {
	pkgdesc="Data files for TORCS"
	cd "$builddir"
	mkdir -p "$subpkgdir"/usr/share/games
	chgrp games "$subpkgdir"/usr/share/games
	make DESTDIR="$subpkgdir" datainstall
}

sha512sums="14b88624803c746b2f356fa185aca5be56b7d7e5192fede11307474100328bfaa67eafa61e84987bbad04590ae5740127e015707723df6d2c3197bb867818101  torcs-1.3.7.tar.bz2
8df4f57b470100c123de5868edc471e4233bf7c8e3d23f759a822536ab28e74403375ecdbcbc404ed442d6820260db97d6c76be7d4fabdef7015722600148b2c  torcs-1.3.6-as-needed.patch
6368ab1df79005f5dede537b45db7ccc1943c2c53e0c696ff6b743680f2d48508e29b5ad56b10a373beddf28c82d5f123998d9e794b66ce49e0ea2226f2332aa  torcs-1.3.6-format.patch
9c29c78c4a2f6d2ea187f80c4c10337556ec13cf7dbe4380af37bccd0fa09a67a6209caff319a50c65f9e831b7d07c1af2c1ba8c33d9a3addc55713988fc3e46  torcs-1.3.6-gcc6.patch
b1908224b82ca3de6694950770a1b70a4935b928b132bd352f67bc3c9b931699b579811f61909b98b28aba19eeb3df9e3d0c4ce8f85caaf4eff3306575efe212  torcs-1.3.6-no-automake.patch
cc4541cfa8440ba044576da18c0eb310ba8d68863a409de8dc8fe73f34828cd035f9995baf535d2e1e639cb17cd8a1d92b2addf7d24d7d79f5b1a746be046b5c  define-uint.patch"
