# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=hyphen
pkgver=2.8.8
pkgrel=1
pkgdesc="Library for high quality word hyphenation and justification"
url="https://hunspell.github.io/"
arch="all"
license="LGPL-2.1+ OR LGPL-2.0+ OR MPL-1.0 or GPL-2.0+"
subpackages="$pkgname-dev"
depends=""
depends_dev="perl"
makedepends="$depends_dev"
source="https://downloads.sourceforge.net/hunspell/$pkgname-$pkgver.tar.gz
	mawk.patch"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-static \
		--disable-dependency-tracking \
		--enable-fast-install
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="$pkgdir" install
	# The en_US dictionary that comes with has an unacceptable license
	# and it isn't really needed anyway since libreoffice has its own
	rm -r "$pkgdir"/usr/share
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

sha512sums="ee514952be56869840b70fb74f60eba14dc4de246733ff8705492367e8cf00c485f8778a9d5a7ba374c988d4ac9fedbe75826dc559e1b62465dbfba21f6ce7de  hyphen-2.8.8.tar.gz
c9ee32c265ded1e862baae69ec30f3a1769b447bdcf0b51d1d59d5fe4c986c8fdf04049085344311f411cd0204a5f7c003eb290669a668d9b137c74698d1c8b0  mawk.patch"
