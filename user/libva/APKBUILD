# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libva
pkgver=2.4.1
pkgrel=0
pkgdesc="Video Acceleration (VA) API for Linux"
url="https://github.com/intel/libva"
arch="all"
options="!check"  # No test suite.
license="MIT"
depends=""
depends_dev="mesa-dev"
makedepends="$depends_dev autoconf automake libtool cmd:which"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/intel/$pkgname/archive/$pkgver.tar.gz"

prepare() {
	default_prepare
	./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-x11 \
		--disable-wayland \
		--disable-static \
		--enable-shared
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="6ad2255aebcc62b3a7f67f03585f8046742a8a9b33c0feaee7dc0a713ab2117b13f154c3574f98aa2fb07fd2336635df3ea2675ec4d1c25ae3b2834aeab8692e  libva-2.4.1.tar.gz"
