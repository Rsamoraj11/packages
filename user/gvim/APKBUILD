# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=gvim
_pkgreal=vim
pkgver=8.1.1635
pkgrel=0
pkgdesc="advanced text editor"
url="http://www.vim.org"
arch="all"
options="!check"  # requires controlling TTY, and fails with musl locales
license="Vim"
depends="vim"
makedepends="acl-dev ncurses-dev libx11-dev perl-dev python3-dev gtk+3.0-dev
	libice-dev libsm-dev libxpm-dev libxt-dev"
source="$_pkgreal-$pkgver.tar.gz::https://github.com/$_pkgreal/$_pkgreal/archive/v$pkgver.tar.gz"
builddir="$srcdir/$_pkgreal-$pkgver"

# secfixes:
#   8.0.0329-r0:
#     - CVE-2017-5953
#   8.0.0056-r0:
#     - CVE-2016-1248

prepare() {
	cd "$builddir"
	# Read vimrc from /etc/vim
	echo '#define SYS_VIMRC_FILE "/etc/vim/vimrc"' >> src/feature.h
}

build() {
	local _onlynative
	cd "$builddir"
	[ "$CBUILD" != "$CHOST" ] || _onlynative="--enable-perlinterp --enable-python3interp=dynamic"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		$_onlynative \
		--with-x \
		--enable-acl \
		--enable-nls \
		--enable-multibyte \
		--enable-gui=gtk3 \
		--disable-ncurses \
		--with-compiledby="Adélie Linux" \
		vim_cv_toupper_broken=no \
		vim_cv_terminfo=yes \
		vim_cv_tgent=zero \
		vim_cv_tty_group=world \
		vim_cv_getcwd_broken=no \
		vim_cv_stat_ignores_slash=no \
		vim_cv_memmove_handles_overlap=yes \
		STRIP=:
	make
}

package() {
	cd "$builddir"

	install -Dm755 src/vim "$pkgdir"/usr/bin/gvim
	install -Dm755 src/gvimtutor "$pkgdir"/usr/bin/gvimtutor
	install -Dm644 runtime/vim16x16.png "$pkgdir"/usr/share/locolor/16x16/apps/gvim.png
	install -Dm644 runtime/vim32x32.png "$pkgdir"/usr/share/locolor/32x32/apps/gvim.png
	install -Dm644 runtime/vim48x48.png "$pkgdir"/usr/share/hicolor/48x48/apps/gvim.png
	install -Dm644 runtime/gvim.desktop "$pkgdir"/usr/share/applications/gvim.desktop

	cd "$pkgdir"/usr/bin
	ln -s gvim gview
	ln -s gvim gvimdiff
	ln -s gvim rgview
	ln -s gvim rgvim
}

sha512sums="41b79ff52438ecbf56366696d3a12d34531a835b64d2ea809597797531950f2f8eb73d753375d23e1411631afaa3f9413fad8af7b0a1fed1532a1714ec1dbb4b  vim-8.1.1635.tar.gz"
