# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: 
pkgname=curl
pkgver=7.65.1
pkgrel=0
pkgdesc="An URL retrival utility and library"
url="https://curl.haxx.se"
arch="all"
license="MIT"
depends="ca-certificates"
makedepends_build="groff perl"
makedepends_host="zlib-dev openssl-dev libssh2-dev"
makedepends="$makedepends_build $makedepends_host"
source="https://curl.haxx.se/download/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-dev libcurl"

# secfixes:
#   7.65.1-r0:
#     - CVE-2019-5435
#     - CVE-2019-5436
#   7.64.0-r0:
#     - CVE-2019-3823
#     - CVE-2019-3822
#     - CVE-2018-16890
#     - CVE-2018-16842
#     - CVE-2018-16840
#     - CVE-2018-16839
#   7.60.0-r0:
#     - CVE-2017-8816
#     - CVE-2017-8817
#     - CVE-2017-8818
#     - CVE-2018-1000005
#     - CVE-2018-1000007
#     - CVE-2018-1000120
#     - CVE-2018-1000121
#     - CVE-2018-1000122
#     - CVE-2018-1000300
#     - CVE-2018-1000301
#   7.56.1-r0:
#     - CVE-2017-1000257
#   7.55.0-r0:
#     - CVE-2017-1000099
#     - CVE-2017-1000100
#     - CVE-2017-1000101
#   7.54.0-r0:
#     - CVE-2017-7468
#   7.53.1-r2:
#     - CVE-2017-7407
#   7.53.0:
#     - CVE-2017-2629
#   7.52.1:
#     - CVE-2016-9594
#   7.51.0:
#     - CVE-2016-8615
#     - CVE-2016-8616
#     - CVE-2016-8617
#     - CVE-2016-8618
#     - CVE-2016-8619
#     - CVE-2016-8620
#     - CVE-2016-8621
#     - CVE-2016-8622
#     - CVE-2016-8623
#     - CVE-2016-8624
#     - CVE-2016-8625
#   7.50.3:
#     - CVE-2016-7167
#   7.50.2:
#     - CVE-2016-7141
#   7.50.1:
#     - CVE-2016-5419
#     - CVE-2016-5420
#     - CVE-2016-5421
#   7.36.0:
#     - CVE-2014-0138
#     - CVE-2014-0139

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-ipv6 \
		--enable-unix-sockets \
		--without-libidn \
		--without-libidn2 \
		--disable-ldap \
		--with-pic
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

libcurl() {
	pkgdesc="The multiprotocol file transfer library"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr
}

sha512sums="aba2d979a416d14a0f0852d595665e49fc4f7bff3bee31f3a52b90ba9dc5ffdb09c092777f124215470b72c47ebca7ddb47844cbf5c0e9142099272b6ac55df4  curl-7.65.1.tar.xz"
