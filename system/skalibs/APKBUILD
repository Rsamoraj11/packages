# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=skalibs
pkgver=2.8.1.0
pkgrel=0
pkgdesc="A set of general-purpose C programming libraries for skarnet.org software"
url="https://skarnet.org/software/skalibs/"
arch="all"
options="!check"  # No test suite.
license="ISC"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	cd "$builddir"
	./configure \
		--enable-shared \
		--enable-static \
		--libdir=/usr/lib \
                --enable-clock
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}


libs() {
	pkgdesc="$pkgdesc (shared libraries)"
	depends=
	mkdir -p "$subpkgdir/lib"
	mv "$pkgdir"/lib/*.so.* "$subpkgdir/lib/"
}


dev() {
	pkgdesc="$pkgdesc (development files)"
	depends=
	mkdir -p "$subpkgdir"
	mv "$pkgdir/usr" "$subpkgdir/"
}


libsdev() {
	pkgdesc="$pkgdesc (development files for dynamic linking)" 
	depends="$pkgname-dev"
	mkdir -p "$subpkgdir/lib"
	mv "$pkgdir"/lib/*.so "$subpkgdir/lib/"
}


doc() {
	pkgdesc="$pkgdesc (documentation)" 
	depends=
	install_if="docs $pkgname=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir/usr/share/doc"
	cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="d2717e6f034af22a7b2ce31b019f30cfa270f64c61549db7b60b319defc5236fc6b58d29a4dae0f740ffdf32180b1940630f4239ea40cbedfcc5861398d20d86  skalibs-2.8.1.0.tar.gz"
