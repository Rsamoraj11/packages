# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sqlite
pkgver=3.28.0
pkgrel=0
pkgdesc="C library that implements an SQL database engine"
url="https://sqlite.org/"
arch="all"
license="Public-Domain"
depends=""
makedepends="libedit-dev zlib-dev"
source="https://sqlite.org/2019/$pkgname-autoconf-3280000.tar.gz
	license.txt"
subpackages="$pkgname-doc $pkgname-dev $pkgname-libs"

builddir="$srcdir/$pkgname-autoconf-3280000"

build() {
	local _amalgamation="-DSQLITE_ENABLE_FTS4 \
	-DSQLITE_ENABLE_FTS3 \
	-DSQLITE_ENABLE_FTS3_PARENTHESIS \
	-DSQLITE_ENABLE_FTS5 \
	-DSQLITE_ENABLE_COLUMN_METADATA \
	-DSQLITE_SECURE_DELETE \
	-DSQLITE_ENABLE_UNLOCK_NOTIFY \
	-DSQLITE_ENABLE_RTREE \
	-DSQLITE_USE_URI \
	-DSQLITE_ENABLE_DBSTAT_VTAB \
	-DSQLITE_MAX_VARIABLE_NUMBER=250000 \
	-DSQLITE_ENABLE_JSON1"
	export CFLAGS="$CFLAGS $_amalgamation"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-threadsafe \
		--enable-static \
		--enable-editline \
		--enable-dynamic-extensions

	# rpath removal
	sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
	sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	install -Dm0644 sqlite3.1 \
		"$pkgdir"/usr/share/man/man1/sqlite3.1
	install -Dm644 "$srcdir"/license.txt \
		"$pkgdir"/usr/share/licenses/$pkgname/license.txt
}

libs() {
	pkgdesc="SQLite embeddable library"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr/
}

sha512sums="e800c0d9e6c8c01ccf1d714c6c4da4b98e9610c4c06557dda6393d0792a8ae09788703d4a74dcb21844c49b3629ff7ed95a4a86ff79872aafd2b49c672c7a570  sqlite-autoconf-3280000.tar.gz
5bde14bec5bf18cc686b8b90a8b2324c8c6600bca1ae56431a795bb34b8b5ae85527143f3b5f0c845c776bce60eaa537624104cefc3a47b3820d43083f40c6e9  license.txt"
