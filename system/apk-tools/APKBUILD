# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
pkgname=apk-tools
pkgver=2.10.4
pkgrel=0
pkgdesc="Alpine Package Keeper - package manager"
url="https://git.alpinelinux.org/cgit/apk-tools/"
arch="all"
license="GPL-2.0-only"
depends="ca-certificates"
makedepends_build=""
makedepends_host="zlib-dev openssl openssl-dev linux-headers"
makedepends="$makedepends_build $makedepends_host"
subpackages="$pkgname-static"
source="https://dev.alpinelinux.org/archive/$pkgname/$pkgname-$pkgver.tar.xz
	pmmx.patch
	search.patch
	virtual.patch
	"

prepare() {
	default_prepare
	cd "$builddir"
	sed -i -e 's:-Werror::' Make.rules
	echo "FULL_VERSION=$pkgver-r$pkgrel" > config.mk
	echo "LUAAPK=" >> config.mk
	echo "export LUAAPK" >> config.mk
}

build() {
	cd "$builddir"
	make
	make static
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	install -d "$pkgdir"/var/lib/apk \
		"$pkgdir"/var/cache/misc \
		"$pkgdir"/etc/apk/keys \
		"$pkgdir"/etc/apk/protected_paths.d
	# the shipped README is empty
	rm -r "$pkgdir"/usr/share/
}

static() {
	pkgdesc="Alpine Package Keeper - static binary"
	install -Dm755 "$srcdir"/$pkgname-$pkgver/src/apk.static \
		"$subpkgdir"/sbin/apk.static

	# lets sign the static binary so it can be vefified from distros
	# that does not have apk-tools
	local abuild_conf=${ABUILD_CONF:-"/etc/abuild.conf"}
	local abuild_home=${ABUILD_USERDIR:-"$HOME/.abuild"}
	local abuild_userconf=${ABUILD_USERCONF:-"$abuild_home/abuild.conf"}
	[ -f "$abuild_userconf" ] && . "$abuild_userconf"
	local privkey="$PACKAGER_PRIVKEY"
	local pubkey=${PACKAGER_PUBKEY:-"${privkey}.pub"}
	local keyname=${pubkey##*/}
	${CROSS_COMPILE}strip "$subpkgdir"/sbin/apk.static
	openssl dgst -sha1 -sign "$privkey" \
		-out "$subpkgdir"/sbin/apk.static.SIGN.RSA.$keyname \
		"$subpkgdir"/sbin/apk.static
}

sha512sums="d2d9fde0aae9059236f68a3fc2f2186104bb9a099b15d296a6202a20ab2912638f10bb3b9edb70f359d060c5839573c3d50ef37d13095fa01c66dc3219ab6e39  apk-tools-2.10.4.tar.xz
78f863c83af8b245aa38416c47016bcbe95ea8e33802123f6ea54b3752eb3c1ee79e5d6fc76ddedd2fba522b82a7367837f4596321085d54397b27fd5edf3b2f  pmmx.patch
5ac09eadabe44fe05c952c25df8f6daca292c63e6e5e3481d57603b8b3f1980385ccd87ad4a87b617353d1f936591afb9ee8815e4d8ec3e33913be2b3e3a3f84  search.patch
4e2a8db82e6e40ae4863d12ee55151468d6e8c383e2e2001015f469e07f47057bc9055ab52a4a04f2bffa41ec225f77a9bd6042d5e1fcbd2908d7393e3aed2a1  virtual.patch"
