# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
# KEEP THIS IN SYNC with the other easy-kernel packages.
_kflavour=""
_pkgname=easy-kernel$_kflavour
pkgver=4.14.127
pkgrel=14
pkgname=$_pkgname-$pkgver-mc$pkgrel
pkgdesc="The Linux kernel, packaged for your convenience"
url="https://kernel.org/"
arch="all"
options="!check !dbg !strip !tracedeps"
license="GPL-2.0-only"
depends=""
makedepends="bc gzip kmod lzop openssl-dev xz"
provides="easy-kernel$_kflavour=$pkgver-r$pkgrel"
subpackages="$_pkgname-modules-$pkgver-mc$pkgrel:modules
	$_pkgname-src-$pkgver-mc$pkgrel:src
	linux-headers:headers"
_pkgmajver=${pkgver%%.*}
_pkgminver=${pkgver%.*}
source="https://cdn.kernel.org/pub/linux/kernel/v${_pkgmajver}.x/linux-${_pkgminver}.tar.xz
	https://distfiles.adelielinux.org/source/linux-${_pkgminver}-mc$pkgrel.patch.xz
	ast-endianness.patch
	config-ppc64
	config-ppc
	config-x86_64
	config-pmmx
	config-aarch64
	config-sparc64
	config-m68k
	kernel.h
	"
builddir="$srcdir/linux-${_pkgminver}"

prepare() {
	cd "$srcdir"
	cat linux-${_pkgminver}-mc$pkgrel.patch.xz | unxz -> linux-${_pkgminver}-mc$pkgrel.patch
	patch -Np1 -d "$builddir" <linux-${_pkgminver}-mc$pkgrel.patch
	default_prepare
	cd "$srcdir"
	cp config-$CARCH linux-${_pkgminver}/.config
	cp -pr linux-${_pkgminver} linux-src
}

build() {
	cd "$builddir"
	make LDFLAGS=""

	cd "$srcdir/linux-src"
	make LDFLAGS="" modules_prepare clean
	cp "$builddir/Module.symvers" .

	# Kernel bug: crtsavres.o is required to build modules, but modules_prepare doesn't create it.
	if [ $CARCH = ppc ]; then
		cp "$builddir/arch/powerpc/lib/crtsavres.o" arch/powerpc/lib/
	fi
}

package() {
	cd "$builddir"
	mkdir -p "$pkgdir"/boot
	make INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		install modules_install

	if [ -f "$pkgdir"/boot/vmlinuz ]; then
		mv "$pkgdir"/boot/vmlinuz \
			"$pkgdir"/boot/vmlinuz-$pkgver-mc$pkgrel-easy$_kflavour
	fi
	if [ -f "$pkgdir"/boot/vmlinux ]; then
		mv "$pkgdir"/boot/vmlinux \
			"$pkgdir"/boot/vmlinux-$pkgver-mc$pkgrel-easy$_kflavour
	fi

	if [ -f "$pkgdir"/boot/System.map ]; then
		mv "$pkgdir"/boot/System.map \
			"$pkgdir"/boot/System.map-$pkgver-mc$pkgrel-easy$_kflavour
	fi

	case $CARCH in
	aarch64|arm*)	make INSTALL_PATH="$pkgdir"/boot dtbs_install ;;
	esac

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/easy-$pkgver-mc$pkgrel$_kflavour/kernel.release
}

modules() {
	pkgdesc="Modules / device drivers for easy-kernel"
	provides="easy-kernel$_kflavour-modules=$pkgver-r$pkgrel"
	autodeps=0  # modules should not depend on src just for symlink
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/modules "$subpkgdir"/lib/
	rm "$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy$_kflavour/build
	rm "$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy$_kflavour/source
	ln -s "../../../usr/src/linux-$pkgver-mc$pkgrel$_kflavour" \
		"$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy$_kflavour/build
	ln -s "../../../usr/src/linux-$pkgver-mc$pkgrel$_kflavour" \
		"$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy$_kflavour/source
}

headers() {
	pkgdesc="System headers provided by the Linux kernel"
	cd "$builddir"
	patch -Np1 -i "$srcdir"/../revert-broken-uapi.patch

	mkdir -p "$subpkgdir"/usr
	make headers_install INSTALL_HDR_PATH="$subpkgdir/usr"

	find "$subpkgdir/usr" \( -name .install -o -name ..install.cmd \) -exec \
		rm -f {} \;

	# provided by libdrm
	rm -rf "$subpkgdir"/usr/include/drm

	# needed for spl, VMware on x86, etc
	install -D -m644 "$builddir"/include/generated/utsrelease.h \
		"$subpkgdir"/usr/include/linux/utsrelease.h

	cp "$srcdir"/kernel.h "$subpkgdir"/usr/include/linux/
}

src() {
	pkgdesc="Kernel source code used to build the kernel"
	provides="easy-kernel$_kflavour-src=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/usr/src
	mv "$srcdir"/linux-src "$subpkgdir"/usr/src/linux-$pkgver-mc$pkgrel$_kflavour
}

sha512sums="77e43a02d766c3d73b7e25c4aafb2e931d6b16e870510c22cef0cdb05c3acb7952b8908ebad12b10ef982c6efbe286364b1544586e715cf38390e483927904d8  linux-4.14.tar.xz
61eda169a4bd195202f1cf82a6d7c8ff10e420cfa1ad67ab8346ed868e154196e9c13002bf4849d6afe6d7115bcd139c6bb395791446ec2f69aa2adebfdf675f  linux-4.14-mc14.patch.xz
e41d9111219342ad13367902242444ecdd847a93575c3f9709d6c6a075bc650f4a15be9db1a8798435fc0a0b56d41705829bfe0d2c2d88f8d1c28931e27ef5be  ast-endianness.patch
0682ba99ea4fced1daa4b39dbc34dbe2e989336cb238f11865e6a9f772e0895aec3f170c041753513fa23eb9c62933215777ff60f3ed363c90bd5e5afadcf1a5  config-ppc64
875d0e1f8d581640c8b584f264ecf7fd750b28af37ae94e5fbf543fbe00b4e09ecd6770a234f026ea81caf7be70ef5049ccb2439916f9f623aa6a7098febe996  config-ppc
6f776cf91d0278f26710ae92fd0386b9fbc234e112a39110c078c28cd9a0911e04c1dbce70c35146ac98230a7e0eb969d6c41ca8c3fe2b127e8826b959989531  config-x86_64
9f4d53407a9714af955d8410358c9b776fc04a73aa560b41325a70113aa2849a5a7c2c61dee6d099e38dae15e071873085a2e50f34c9535b390a8e13edc80fc0  config-pmmx
073d7d4b0f39858ed20f027198caf1f1cea7402472903a50b9af94de5759a4cc6d81c6375de7642a41d57cbc87cf301e0bc79ceeaa6375e4dfa9df9c0e782f45  config-aarch64
5cd48d4b2a9c5f0724bb30c7f3d6f15ab609f115e9f2f2b96d33ff43e387cb4719283591a91bdd08ca04543c8114c2bb47dbf3695366cde2dce7791029f68d23  config-sparc64
281e422f1854f88ad8b2863b1f5de9d4f20cd5a2eee0383422449230e1972db5a9a8cd1c03a9b61e2a78410637ce3991bb9a4c61fd332c49b7e54bd59f7db710  config-m68k
fdd94b9de7b374f1812dec0f9971c05f5e52177ca4a16c8071daa620c18265b38f656f528e7045a24f30af01f05e53e54546fb0a9e93773cf191866e87de505d  kernel.h"
