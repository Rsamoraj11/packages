# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tzdata
pkgver=2019a
_tzcodever=2019a
_ptzver=0.5
pkgrel=0
pkgdesc="Time zone data"
url="https://www.iana.org/time-zones"
arch="all"
options="!check"  # Until we have sgml-common.
license="Public-Domain"
depends=""
makedepends=""
checkdepends="sgml-common sp"
subpackages="$pkgname-doc"
source="https://www.iana.org/time-zones/repository/releases/tzcode$_tzcodever.tar.gz
	https://www.iana.org/time-zones/repository/releases/tzdata$pkgver.tar.gz
	https://dev.alpinelinux.org/archive/posixtz/posixtz-$_ptzver.tar.xz
	0001-posixtz-fix-up-lseek.patch"

builddir="$srcdir"
_timezones="africa antarctica asia australasia europe northamerica \
	southamerica pacificnew etcetera backward systemv factory"

build() {
	make cc="${CC:-gcc}" CFLAGS="$CFLAGS -DHAVE_STDINT_H=1"
		TZDIR="/usr/share/zoneinfo"

	make -C posixtz-$_ptzver posixtz
}

check() {
	make -j1 check
}

package() {
	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo ${_timezones}
	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo/right -L leapseconds ${_timezones}
	#./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo/posix ${_timezones}

	./zic -y ./yearistype -d "$pkgdir"/usr/share/zoneinfo -p America/New_York
	install -m444 -t "$pkgdir"/usr/share/zoneinfo iso3166.tab zone1970.tab zone.tab

	mkdir -p "$pkgdir"/usr/sbin
	install -m755 zic zdump "$pkgdir"/usr/sbin

	mkdir -p "$pkgdir"/usr/share/man/man8
	install -m644 zic.8 zdump.8 "$pkgdir"/usr/share/man/man8

	rm -f "$pkgdir"/usr/share/zoneinfo/localtime
	install -Dm755 "$srcdir"/posixtz-$_ptzver/posixtz \
		"$pkgdir"/usr/bin/posixtz
}

sha512sums="7cc76ce6be4a67c3e1b2222cb632d2de9dabb76899793a938f87a1d4bb20e462cabdae9e3b986aaabaa400795370510095d236dbad5aff4c192d0887f0ecedf5  tzcode2019a.tar.gz
d8eb5b2b68abee08bd2b0d2134bce85b5c0aee85168e9697a607604ed5be7d1539ac60fda9b37e0c9c793ef6251978bc250563a0af59497fde775499964bb5aa  tzdata2019a.tar.gz
68dbaab9f4aef166ac2f2d40b49366527b840bebe17a47599fe38345835e4adb8a767910745ece9c384b57af815a871243c3e261a29f41d71f8054df3061b3fd  posixtz-0.5.tar.xz
f54ce213d74c5a8387e1a7c56299bc6eee65a035772288222128abc249a112067b8791b88b45c342b2d4d8d12e9e4f1f2f5c92c5de67f8b6413b1ebf1d7de467  0001-posixtz-fix-up-lseek.patch"
