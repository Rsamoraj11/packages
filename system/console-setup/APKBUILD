# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=console-setup
pkgver=1.191
pkgrel=1
pkgdesc="Set up console font and keyboard layout"
url="https://salsa.debian.org/installer-team/console-setup"
arch="noarch"
options="!check"  # No test suite.
license="MIT AND GPL-2.0+ AND BSD-3-Clause"
depends="ckbcomp kbd"
makedepends="perl"
subpackages="$pkgname-doc console-fonts:fonts console-keymaps:keymaps
	$pkgname-openrc ckbcomp"
source="http://ftp.de.debian.org/debian/pool/main/c/$pkgname/${pkgname}_$pkgver.tar.xz
	console-setup.initd
	"

build() {
	make build-linux
}

package() {
	make prefix="$pkgdir/usr" etcdir="$pkgdir/etc" xkbdir= install-linux
	install -D -m755 "$srcdir"/console-setup.initd "$pkgdir"/etc/init.d/console-setup
}

fonts() {
	pkgdesc="Console fonts for use with $pkgname"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/consolefonts "$subpkgdir"/usr/share/
}

keymaps() {
	pkgdesc="Keyboard layouts for use with $pkgname"
	mkdir -p "$subpkgdir"/etc/console-setup
	mv "$pkgdir"/etc/console-setup/ckb "$subpkgdir"/etc/console-setup/
}

ckbcomp() {
	pkgdesc="XKB keyboard layout translation utility"
	depends="perl"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/ckbcomp "$subpkgdir"/usr/bin/
}

sha512sums="a56a6db65a5603832770f7175db95468a213f39f324d78235cb8f4d88aaa94cd8eda1aae22a87082aa9ef76466c8bc7b113c43583fb74134013abb7ee6d83389  console-setup_1.191.tar.xz
3b8e2c9d8551f9a51bcd33e58771a4f55ff2840f8fe392e0070bd2b6a3911cd9ed9377873538f6904fd99836ac4e0280c712be69d275aae9183dd12ff7efddae  console-setup.initd"
