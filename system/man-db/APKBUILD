# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=man-db
pkgver=2.8.5
pkgrel=0
pkgdesc="The man command and related utilities for examining on-line help files"
url="https://www.nongnu.org/man-db/"
arch="all"
options="!check"  # requires //IGNORE in iconv
license="GPL-2.0+"
depends="groff gzip less"
makedepends_host="db-dev libpipeline-dev zlib-dev"
subpackages="$pkgname-doc $pkgname-lang"
triggers="man-db.trigger=/usr/share/man"
source="https://download.savannah.nongnu.org/releases/man-db/man-db-$pkgver.tar.xz
	man-db-2.8.5-iconv.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-setuid \
		--with-sections="1 1p 1x 2 2x 3 3p 3x 4 4x 5 5x 6 6x 7 7x 8 8x 9 0p tcl n l p o" \
		--enable-nls \
		--with-db=db
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	rm -r "${pkgdir}"/usr/lib/tmpfiles.d  # systemd
}

sha512sums="8d1524c3b6459c9ac02d969149c18c198cb171bcd6acc493e863a466c01309958ee9f5ac52df4d7d27da29d35cb7c64132732f5b969181ab336500df2e6dad69  man-db-2.8.5.tar.xz
76a5b13d3018627cbc5a72cb51da3c78022245ad574c742699e7c4ab152ec2222ce7f34af5594de74b436dd23008af6c43739ee66973e049b07d82882e4965c6  man-db-2.8.5-iconv.patch"
