# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=file
pkgver=5.37
pkgrel=0
pkgdesc="File type identification utility"
url="http://www.darwinsys.com/file/"
arch="all"
license="BSD-2-Clause"
subpackages="$pkgname-dev $pkgname-doc libmagic"
source="$pkgname-$pkgver.tar.gz::https://github.com/file/file/archive/FILE${pkgver%.*}_${pkgver#*.}.tar.gz"
builddir="$srcdir/$pkgname-FILE${pkgver%.*}_${pkgver#*.}"

prepare() {
	cd "$builddir"
	autoreconf -vif
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--datadir=/usr/share
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

libmagic() {
	pkgdesc="File type identification library"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$pkgdir"/usr/share "$subpkgdir"/usr
}

sha512sums="9b6ae3dd910a03d2161c91ebc75ac91eb7dbd279563462b77daf902d9ae9f0a70de12c37a498b20c6357d6594059d01841bfd104592107b65c08d8343fca19d2  file-5.37.tar.gz"
